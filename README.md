# vacation-manager

React project allowing you to manage vacation days for your employees


## Team Members
* Philip Aubert
* Ole Baadshaug
* Daniel Steen Losvik
* Jakob Hauge Moe


## Project Setup

```
git clone git@gitlab.com:daniel_losvik/vacation-manager.git
or
git clone https://gitlab.com/daniel_losvik/vacation-manager.git

cd vacation-manager

npm install

change src/redux/actions/userActions.ts line 14
from redirectUri: "https://www.tidsbankenfuncs.ninja"
to redirectUri: "http://localhost:3000"

npm start
```


