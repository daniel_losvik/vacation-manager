import React from 'react';
import { Provider } from "react-redux";
import App from './App';
import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from "./redux/rootReducer";
import { BrowserRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
