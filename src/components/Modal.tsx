import React, { useState, useEffect, useCallback } from 'react';
import DatePicker from "react-datepicker";
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { User } from '../types';
import '../styles/modal.css';
import { fetchCurrentUser} from '../redux/actions/userActions';
import { postNewIneligiblePeriod, postUserIneligiblePeriod, setNewIneligiblePeriodToNull } from '../redux/actions/ineligiblePeriodAction'
import { 
    Button,
    Modal, 
    ModalHeader, 
    Label,
    ModalBody, 
    ModalFooter,  
    Form, 
    FormGroup,
    Input
} from 'reactstrap';


const ModalIneligible = (props: any) => {
  const {
    buttonLabel ="Create Ineligible Period",
  } = props as any;
  const [modal, setModal]= useState(false);
  const [stop, setStop]= useState(false);
  const [startDate, setstartDate] = useState(new Date());
  const [endDate, setendDate] = useState(new Date());
  
  const toggle = useCallback(() => {
    setModal(!modal)
  }, [modal]);

  useEffect(() =>{
    if (props.newIneligiblePeriod && !stop){
      setStop(true)
      props.users.forEach((user: User)=>{
        if((document.getElementById(user.Id.toString())) !== null){
        var inputValue:any = (document.getElementById(user.Id.toString()));
          if(inputValue.checked){
            props.dispatch(postUserIneligiblePeriod(user.Id,props.newIneligiblePeriod.Id))
          }
        }
      })
      props.dispatch(setNewIneligiblePeriodToNull())
      props.updateIneligiblePeriodsInCalendar()
      toggle();
    }
  }, [props, stop, toggle]);


  let submitPeriod = (_props: any) =>{
    _props.preventDefault();
   props.dispatch(fetchCurrentUser())
   props.dispatch(postNewIneligiblePeriod(props.currentUser.Moderator.Id,startDate,endDate))
  }

  return (
    <div className ="ineligible-perdiod-container">
      <Form inline onSubmit={(e) => e.preventDefault()}>
        <FormGroup className="mx-2" check>
        <Button color="secondary" onClick={toggle}>{buttonLabel}</Button>
        </FormGroup>
        
      </Form>
      <Modal isOpen={modal} toggle={toggle} className="new-vacation-request-form">
        <ModalHeader className="modal-header" toggle={toggle}>Ineligible Period</ModalHeader>
        <ModalBody className ="modal-body">
        <Label className="new-ineligible-period-label-from">From</Label>
        <DatePicker className="new-ineligible-period-datepicker-from" selected={startDate} onChange={(date: Date) =>{setstartDate(date);}} />
        {" "}
        <br/>
        <Label className="new-ineligible-period-label-to">To</Label>
        <DatePicker className="new-ineligible-period-datepicker-to" selected={endDate} onChange={(date: Date) =>{setendDate(date);}}/>
​         <br/>
​
      {props.users.map((user: any, i: number) =>(
          <Label checked key={i}  >
           <br/>
            <Input 
            type ="checkbox"
            id={user.Id}
            />
               {user.FirstName+" "}{user.LastName}
          </Label>
          ))}
        </ModalBody>
        <ModalFooter>
          <Button type="submit" color="primary" onClick={submitPeriod}>Submit</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}
function mapStateToProps(state: any) {
    return {
      users: state.userState.users,
      newIneligiblePeriod: state.ineligiblePeriodState.newIneligiblePeriod,
      newIneligiblePeriodError: state.ineligiblePeriodState.newIneligiblePeriodError,
      currentUser: state.userState.currentUser
      
    };
  }
export default connect(mapStateToProps)(withRouter(ModalIneligible));