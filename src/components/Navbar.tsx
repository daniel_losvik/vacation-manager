import React, { useState } from 'react';
import { connect } from 'react-redux';
import { logout } from '../redux/actions/userActions';
import { Navbar as NavBar} from 'reactstrap';
import { withRouter } from "react-router";
import { User, VacationRequest } from '../types';
import {
    Collapse,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
  } from 'reactstrap';
  

function Navbar(props: any) {

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

    return (
      <div>
      <NavBar color="light" light expand="md">
        <NavbarBrand href="/" onClick={ (e) => {e.preventDefault(); props.history.push("/")}}>Vacation Manager</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/" onClick={ (e) => {e.preventDefault(); props.history.push("/NewVacationRequest")}}>New Vacation Request</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/" onClick={ (e) => {e.preventDefault(); props.history.push("/Users")}}>Browse Users</NavLink>
            </NavItem>
          </Nav>
          <Nav className="ml-auto" navbar>
            <NavItem style={{position:"relative"}}>
              <NavLink 
                href="/"
                onClick={ (e) => {e.preventDefault(); props.history.push("/Admin/Dashboard")}} 
                style={{display: props.currentUser.IsAdmin ? "block" : "none"}}>
                  Admin Page
                  <svg 
                    style={{position:"absolute", left:"-5px", color:"orange", transform:"scale(1.5)", display: anyUserHasPendingRequests(props.users) ? 'inline-block' : 'none'}} 
                      className="bi bi-exclamation exclamation-mark" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.002 11a1 1 0 112 0 1 1 0 01-2 0zM7.1 4.995a.905.905 0 111.8 0l-.35 3.507a.552.552 0 01-1.1 0L7.1 4.995z"/>
                  </svg>
              </NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                {props.currentUser.FirstName + " " + props.currentUser.LastName}
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <NavLink href="/" onClick={ (e) => {e.preventDefault(); props.history.push("/Profile")}}>Profile</NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink href="/" onClick={ (e) => {props.dispatch(logout())}}>Logout</NavLink>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </NavBar>
    </div>
    );
  
}


export function anyUserHasPendingRequests(users: User[]) {
  let hasPendingRequests = false
  if(users) {
    users.forEach((user: User) => {
      if(user.VacationRequests) {
        user.VacationRequests.forEach((vacationRequest: VacationRequest) => {
          if(vacationRequest.Status === "Pending") {
            hasPendingRequests = true
          }
        })
      }
    });
  }
  return hasPendingRequests
}




function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser,
    users: state.userState.users,
  };
}
export default connect(mapStateToProps)(withRouter(Navbar));
