
export interface User {
  Id: number,
  FirstName: string,
  LastName: string,
  Email: string
  IsAdmin: boolean,
  ProfilePic: string,
  Comments: [],
  UserIneligiblePeriods: [],
  VacationRequests: [],
  Moderator: Moderator
}

export interface Moderator {
  Id: number,
  UserId: number,
  User: User,
  IneligiblePeriods: [],
  VacationRequests: []
}

export interface VacationRequest {
  Id: number,
  Title: string,
  PeriodStart: Date,
  PeriodEnd: Date,
  TimeOfModeration: Date,
  UserId: number,
  User: User,
  Moderator: Moderator,
  ModeratorId: number,
  Status: string

}

export interface Comment {
  UserId: number,
  Message: string,
  Created: Date,
  LastEdited: Date,
  User: User,
  VacationRequestId : number,
  VacationRequest: VacationRequest
}

export interface IneligiblePeriod {
  Id: number,
  PeriodStart: Date,
  PeriodEnd: Date
  Status: string,
  ModeratorId: number,
  Moderator: Moderator,
  UserIneligiblePeriods: []
}

export interface UserIneligiblePeriod {
  UserId: number,
  IneligiblePeriodId : number,
}

