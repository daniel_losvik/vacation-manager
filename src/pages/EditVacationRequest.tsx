import React from 'react';
import '../styles/editVacationRequest.css';
import { Spinner, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { VacationRequest, User, UserIneligiblePeriod, IneligiblePeriod } from '../types';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { updateVacationRequest, deleteVacationRequest, setVacationRequestError, setUpdatedVacationRequestNull } from '../redux/actions/vacationRequestActions';
import { fetchUsers, fetchCurrentUser } from '../redux/actions/userActions';
import { fetchIneligiblePeriods } from '../redux/actions/ineligiblePeriodAction';


class EditVacationRequest extends React.Component<any, any> {
  unEditedVacationRequest: any;
  constructor(props: any) {
    super(props);
    let owner = this.getVacationRequestOwnerFromURL(props)
    if(owner) {
      let vacationRequest = owner.VacationRequests.filter((vacationRequest: VacationRequest) => vacationRequest.Id.toString() === this.props.match.params.vrId)[0]
      this.state = {
          Id: vacationRequest.Id,
          PeriodStart: new Date(vacationRequest.PeriodStart),
          PeriodEnd: new Date(vacationRequest.PeriodEnd),
          Title: vacationRequest.Title,
          Status: vacationRequest.Status,
          UserId: vacationRequest.UserId,

          showEditButton: true,
          showSaveButton: false,
          disableButtons: false,
          disableSaveButton: false,
          disableInputFields: true,
      };
      // Create a copy of the unedited vecationrequest
      this.unEditedVacationRequest = JSON.parse(JSON.stringify(vacationRequest))
    }
  }

  componentDidMount() {
    this.props.dispatch(fetchUsers())
    this.props.dispatch(fetchCurrentUser())
    this.props.dispatch(fetchIneligiblePeriods())
    this.props.dispatch(setVacationRequestError(""))
    this.props.dispatch(setUpdatedVacationRequestNull())
  }


  render() {
    if(this.state) {
      return (
        <div className="edit-vacation-request-card">
          <h1 className="edit-vacation-request-header">Edit Vacation Request</h1>
  
          <form onSubmit={this._updateVacationRequest.bind(this)} className="edit-vacation-request-form">
  
            <Label className="edit-vacation-request-label-from">From</Label>
            <DatePicker 
              className="edit-vacation-request-datepicker-from" 
              selected={this.state.PeriodStart} 
              onChange={(date: Date) =>{this.setState({PeriodStart: this.formatDate(date)}, () => this.validateDates())}}
              disabled={this.state.disableInputFields}
              />
            
            <Label className="edit-vacation-request-label-to">To</Label>
            <DatePicker 
              className="edit-vacation-request-datepicker-to" 
              selected={this.state.PeriodEnd} 
              onChange={(date: Date) =>{this.setState({PeriodEnd: this.formatDate(date)}, () => this.validateDates())}}
              disabled={this.state.disableInputFields}/>
  
            <Input 
            type="text" 
            name="title" 
            id="title" 
            className="edit-vacation-request-input-field" 
            placeholder="Title" 
            value={this.state.Title}
            onChange={(event) => this.setState({Title: event.target.value})}
            disabled={this.state.disableInputFields}
            required/>
  
            <div className="edit-vacation-request-success-message" style={{display: this.props.updatedVacationRequest ? 'block' : 'none' }}>Updated</div>
            <div className="edit-vacation-request-error-message">{this.props.updateVacationRequestError}</div>
            <div className="edit-vacation-request-error-message">{this.props.deleteVacationRequestError}</div>
              
            <Spinner color="primary" className={this.props.updateVacationRequestIsLoading || this.props.deleteVacationRequestIsLoading ? "edit-vacation-request-spinner-show" : 'edit-vacation-request-spinner-hide' }/>
  
            <div className="edit-vacation-request-button-container" style={{display: this.state.showEditButton ? 'block' : 'none' }}>
              <button className="btn btn-primary edit-vacation-request-edit-btn" onClick={this.edit.bind(this)} disabled ={this.state.disableButtons}>Edit</button>
              <button className="btn btn-danger edit-vacation-request-delete-btn" onClick={this._deleteVacationRequest.bind(this)} disabled ={this.state.disableButtons} style={{display: this.props.currentUser.Id !== this.state.UserId ? 'span' : 'none' }}>Delete</button>
            </div>
            <div className="edit-vacation-request-button-container" style={{display: this.state.showSaveButton ? 'block' : 'none' }}>
              <button className="btn btn-danger edit-vacation-request-cancel-btn" onClick={this.cancelEdit.bind(this)} disabled ={this.state.disableButtons}>Cancel</button>
              <input type="submit"  className="btn btn-success edit-vacation-request-save-btn" value="Save" disabled ={this.state.disableButtons || this.state.disableSaveButton}></input>
            </div>
            <br/>
            <br/>
            
          </form>
        </div>
      );
    }
    else {
      return (<div></div>)
    }
  }

  componentDidUpdate(propsBeforeUpdate: any) {
    if (this.vacationRequestUpdateWasSuccessfull(propsBeforeUpdate)) {
      this.props.dispatch(fetchCurrentUser())
      this.props.dispatch(fetchUsers())
      this.updatePage()
    }
    if(this.vacationRequestDeleteWasSuccessfull(propsBeforeUpdate)) {
      this.props.history.goBack()
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if(nextProps.users !== this.props.users || nextProps.currentUser !== this.props.currentUser) {
      let owner = this.getVacationRequestOwnerFromURL(nextProps)
      if(owner) {
        let vacationRequest = owner.VacationRequests.filter((vacationRequest: VacationRequest) => vacationRequest.Id.toString() === nextProps.match.params.vrId)[0]
        this.setState({
            Id: vacationRequest.Id,
            PeriodStart: new Date(vacationRequest.PeriodStart),
            PeriodEnd: new Date(vacationRequest.PeriodEnd),
            Title: vacationRequest.Title,
            Status: vacationRequest.Status,
            UserId: vacationRequest.UserId,

            showEditButton: this.state && this.state.showEditButton !== null ? this.state.showEditButton : true,
            showSaveButton: this.state && this.state.showSaveButton !== null ? this.state.showSaveButton : false,
            disableButtons: this.state && this.state.disableButtons !== null ? this.state.disableButtons : false,
            disableSaveButton: this.state && this.state.disableSaveButton !== null ? this.state.disableSaveButton : false,
            disableInputFields: this.state && this.state.disableInputFields !== null ? this.state.disableInputFields : true,
        });
        // Create a copy of the unedited vecationrequest
        this.unEditedVacationRequest = JSON.parse(JSON.stringify(vacationRequest))
      }
    }
  }

  formatDate(date: Date) {
    return new Date(date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate())
  }

  getVacationRequestOwnerFromURL(props: any) {
    if(props.location.pathname.includes("Admin")) {
      return props.users.filter((user: User) => user.Id.toString() === props.match.params.id)[0]
    }
    else {
      return props.currentUser
    }
  }

  _updateVacationRequest(e: any) {
    e.preventDefault()
    this.disableButtonsAndInputFields()
    this.props.dispatch(updateVacationRequest(this.state.Id, this.props.currentUser.Id, this.state.Title, this.state.PeriodStart, this.state.PeriodEnd, this.state.Status));
  }

  _deleteVacationRequest(e: any) {
    e.preventDefault()
    if(window.confirm("Are You Sure You Want To Delete The Vacation Request. This Action Cannot Be Undone")) {
      this.disableButtonsAndInputFields()
      this.props.dispatch(deleteVacationRequest(this.state.Id))
    }
  }

  disableButtonsAndInputFields() {
    this.setState({
      disableButtons: true,
      disableInputFields: true,
    })
  }

  edit(e: any) {
    e.preventDefault();
    this.setState({
      showEditButton: false,
      showSaveButton: true,
      disableInputFields: false
    })
    this.props.dispatch(setUpdatedVacationRequestNull())
  }

  cancelEdit(e: any = new Event("")) {
    e.preventDefault();
    this.setState({
      Id: this.unEditedVacationRequest.Id,
      PeriodStart: new Date(this.unEditedVacationRequest.PeriodStart),
      PeriodEnd: new Date(this.unEditedVacationRequest.PeriodEnd),
      Title: this.unEditedVacationRequest.Title,

      showEditButton: true,
      showSaveButton: false,
      disableButtons: false,
      disableSaveButton: false,
      disableInputFields: true
    })
    this.props.dispatch(setVacationRequestError(""))
  }
  
  vacationRequestUpdateWasSuccessfull(propsBeforeUpdate: any) {
    return ((this.props.updatedVacationRequest && this.props.updatedVacationRequest !== propsBeforeUpdate.updatedVacationRequest) 
      && (this.props.updateVacationRequestError === null));
  }

  vacationRequestDeleteWasSuccessfull(propsBeforeUpdate: any) {
    return this.props.deleteVacationRequestWasSuccessfull && this.props.deleteVacationRequestWasSuccessfull !== propsBeforeUpdate.deleteVacationRequestWasSuccessfull
  }

  updatePage() {
    this.unEditedVacationRequest = JSON.parse(JSON.stringify(this.props.updatedVacationRequest))
    this.setState({
      showEditButton: true,
      showSaveButton: false,
      disableButtons: false,
      disableInputFields: true
    })
  }

  enableSaveButton() {
    this.setState({
      disableSaveButton: false,
    })
  }
  
  disableSaveButton() {
    this.setState({
      disableSaveButton: true,
    })
  }

  validateDates() {
    if(this.state.PeriodStart && this.state.PeriodEnd) {
      this.enableSaveButton()
      this.props.dispatch(setVacationRequestError(""))

      if(this.state.PeriodStart > this.state.PeriodEnd) {
        this.props.dispatch(setVacationRequestError("Invalid Dates"))
        this.disableSaveButton()
      }
      else if(this.props.currentUser.UserIneligiblePeriods && this.props.ineligiblePeriods) {
        this.props.currentUser.UserIneligiblePeriods.forEach((userIneligiblePeriod: UserIneligiblePeriod) => {

          this.props.ineligiblePeriods.forEach((ineligiblePeriod: IneligiblePeriod) => {
            if(userIneligiblePeriod.IneligiblePeriodId === ineligiblePeriod.Id) {

              if((this.state.PeriodStart <= new Date(ineligiblePeriod.PeriodStart) &&  new Date(ineligiblePeriod.PeriodStart) <= this.state.PeriodEnd)
                || (this.state.PeriodStart <= new Date(ineligiblePeriod.PeriodEnd) &&  new Date(ineligiblePeriod.PeriodEnd) <= this.state.PeriodEnd)
                || (this.state.PeriodStart >= new Date(ineligiblePeriod.PeriodStart) &&  new Date(ineligiblePeriod.PeriodEnd) >= this.state.PeriodEnd))
              {
              this.props.dispatch(setVacationRequestError("Invalid Dates"))
              this.disableSaveButton()
              }
            }
          })
        })
      }
    }
  }
}





function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser,

    users: state.userState.users,

    ineligiblePeriods: state.ineligiblePeriodState.ineligiblePeriods,

    updatedVacationRequest: state.vacationRequestState.updatedVacationRequest,
    updateVacationRequestIsLoading: state.vacationRequestState.updateVacationRequestIsLoading,
    updateVacationRequestError: state.vacationRequestState.updateVacationRequestError,

    deleteVacationRequestIsLoading: state.vacationRequestState.deleteVacationRequestIsLoading,
    deleteVacationRequestError: state.vacationRequestState.deleteVacationRequestError,
    deleteVacationRequestWasSuccessfull: state.vacationRequestState.deleteVacationRequestWasSuccessfull,
  };
}
export default connect(mapStateToProps)(withRouter(EditVacationRequest));


