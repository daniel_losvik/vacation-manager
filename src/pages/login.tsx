import React from 'react';
import '../styles/login.css';
import { Spinner } from 'reactstrap';
import { connect } from 'react-redux';
import { requestLogin } from '../redux/actions/userActions';


class Login extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }


  requestLogin(e: any) {
    e.preventDefault();
    this.props.dispatch(requestLogin());
  }

  render() {
    return (
      <div className="login-card">
        <img src={require('../assets/tidsbanken_logo.png')} alt="Tidsbanken Logo" className="login-logo"/>
        <div className="title">Vacation Manager</div>
        
        <form onSubmit={this.requestLogin.bind(this)} className="login-form">
          
          <div className="login-error-message">{this.props.requestLoginError}</div>

          <Spinner color="primary" className={this.props.requestLoginIsLoading ? "login-spinner-show" : 'login-spinner-hide' }/>
          
          <input type="submit"  className="btn btn-primary login-btn" value="Sign In"></input>
        </form>
      </div>
    );
  }
}





function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser,
    requestLoginIsLoading: state.userState.requestLoginIsLoading,
    requestLoginError: state.userState.requestLoginError,
  };
}
export default connect(mapStateToProps)(Login);


