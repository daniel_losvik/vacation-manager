import React from 'react';
import { Spinner } from 'reactstrap';
import { ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';
import '../styles/calendar.css';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import 'react-perfect-scrollbar/dist/css/styles.css';
import Modal from '../components/Modal';
import FullCalendar from '@fullcalendar/react'
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid'
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import PerfectScrollbar from 'react-perfect-scrollbar'
import { User, VacationRequest, Comment, UserIneligiblePeriod, IneligiblePeriod } from '../types';
import { updateVacationRequest, fetchVacationRequests } from '../redux/actions/vacationRequestActions';
import { fetchUsers, fetchCurrentUser } from '../redux/actions/userActions';
import { postNewComment, fetchAllCommentsForUser, setFetchCommentIsLoading } from '../redux/actions/commentActions';
import { fetchIneligiblePeriods, deleteIneligiblePeriod } from '../redux/actions/ineligiblePeriodAction';


class Calendar extends React.Component<any, any> {
  calendarRef = React.createRef()
  constructor(props: any) {
    super(props);
    if(this.isHomePage()) {
      this.state = {
        textareaInput: "",
        showCommentInputFieldNr: -1,
        disableCommentButtons: false,
        showErrorMessage: false,
        user: this.props.currentUser,
      }
    }
    else {
      this.state = {
        textareaInput: "",
        showCommentInputFieldNr: -1,
        disableCommentButtons: false,
        showErrorMessage: false,
        user: this.props.users.filter((user: User) => user.Id.toString() === this.props.match.params.id)[0]
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(fetchUsers())
    this.props.dispatch(fetchCurrentUser())
    this.props.dispatch(fetchIneligiblePeriods())
    this.props.dispatch(setFetchCommentIsLoading(false))
    if(this.state.user && !this.isAnotherUsersCalendar()) {
      this.props.dispatch(fetchAllCommentsForUser(this.state.user))
    }
    this.props.dispatch(fetchVacationRequests())
  }


  render() {
    if(this.state.user) {
      return (
        <div className="calendar-page-container" >
          <div className="calendar-page-calendar">
            <div className="calendar-page-modal" style={{display: this.props.currentUser && this.props.currentUser.IsAdmin ? "block" : "none"}}>
              <div style={{display: this.isAnotherUsersCalendarFromAdminConsole() ? "block" : "none"}}>
                  <Modal updateIneligiblePeriodsInCalendar={this.updateIneligiblePeriodsInCalendar.bind(this)} />
              </div>
            </div>
            <FullCalendar 
              defaultView = "dayGridMonth" 
              plugins = {[ dayGridPlugin, interactionPlugin ]} 
              header = {{
                left:   'title',
                center: '',
                right:  'prev, next'
              }}
              events = {this.createEvents()}
              dateClick = {(info: any) => {this.createPopupIfUserClickedOnIneligiblePeriod(info)}}
              />
          </div>
          
          <div className="calendar-page-sidebar">
          <PerfectScrollbar>
            <ListGroup>
            <ListGroupItem className="calendar-page-sidebar-profile-info">
              <img src={this.state.user.ProfilePic !== "" ? this.state.user.ProfilePic : require('../assets/profile_picture.png')} alt="Profile" className="calendar-page-sidebar-profile-img"/>
              <ListGroupItemHeading style={{position:"relative", top: "2px"}}>
                {this.state.user.FirstName + " " + this.state.user.LastName}
              </ListGroupItemHeading>
              <Spinner color="primary" className={this.props.updateVacationRequestIsLoading || this.props.postNewCommentIsLoading || this.props.deleteIneligiblePeriodIsLoading ? "calendar-page-spinner-show" : 'calendar-page-spinner-hide' }/>
            </ListGroupItem>

            <this.renderVacationRequests parentComponent={this} />

            </ListGroup>
            </PerfectScrollbar>
          </div>
        </div>
      );
    }
    else {
      return (<div></div>)
    }
  }


  renderVacationRequests(props: any) {
    if(props.parentComponent.state.user.VacationRequests) {

      let VacationRequests = props.parentComponent.state.user.VacationRequests
      if(props.parentComponent.isAnotherUsersCalendar()) {
        VacationRequests = props.parentComponent.state.user.VacationRequests.filter((vacationRequest: VacationRequest) => vacationRequest.Status === "Approved")
      }
      return (
        VacationRequests.slice().reverse().map((vacationRequest: VacationRequest, i: number) => {
          return (
            <ListGroupItem key={i} className="calendar-page-sidebar-vacation-request" style={{color:vacationRequest.Status === "Pending" ? "orange" :vacationRequest.Status === "Approved" ? "green" : "red"}}>
              <div>
                <ListGroupItemHeading>{vacationRequest.Title}</ListGroupItemHeading>
                <ListGroupItemText>
                  {new Date(vacationRequest.PeriodStart).toString().split(":")[0].slice(0, new Date(vacationRequest.PeriodStart).toString().split(":")[0].length - 2)} - {new Date(vacationRequest.PeriodEnd).toString().split(":")[0].slice(0, new Date(vacationRequest.PeriodStart).toString().split(":")[0].length - 2)}
                </ListGroupItemText>
              </div>
              <div className="calendar-page-sidebar-btn-container">
                <button 
                  className="btn btn-success calendar-page-sidebar-btn" 
                  value={vacationRequest.Id} 
                  onClick={() => props.parentComponent.acceptVactionRequest(vacationRequest)}
                  style={{display: props.parentComponent.isAnotherUsersCalendarFromAdminConsole() ? "span" : "none"}}>
                    Approve
                </button>
                <button 
                  className="btn btn-danger calendar-page-sidebar-btn" 
                  value={vacationRequest.Id} 
                  onClick={() => props.parentComponent.rejectVactionRequest(vacationRequest)}
                  style={{display: props.parentComponent.isAnotherUsersCalendarFromAdminConsole() ? "span" : "none"}}>
                    Decline
                </button>
                <button 
                  className="btn btn-primary calendar-page-sidebar-btn"
                  onClick={ (e) => {props.parentComponent.navigateToEditVacationRequestPage(e, vacationRequest)}}
                  style={{display: (props.parentComponent.isHomePage() && vacationRequest.Status === "Pending") || props.parentComponent.isAnotherUsersCalendarFromAdminConsole() ? "span" : "none"}}>
                    Edit
                </button>
                <button 
                  className="btn btn-primary calendar-page-sidebar-btn"
                  onClick={ (e) => {props.parentComponent.setState({textareaInput: "", showCommentInputFieldNr: props.parentComponent.state.showCommentInputFieldNr === i ? -1 : i, showErrorMessage: false})}}
                  style={{display: props.parentComponent.isHomePage() || props.parentComponent.isAnotherUsersCalendarFromAdminConsole() ? "span" : "none"}}>
                    Comment
                </button>
              </div>
              <form onSubmit={e => props.parentComponent._postComment(e, vacationRequest)} className="calendar-page-sidebar-comment-section">
                <div style={{display: props.parentComponent.state.showCommentInputFieldNr === i ? "block" : "none"}}>
                  <textarea 
                    rows={2} 
                    placeholder="Add a comment..." 
                    className="calendar-page-sidebar-comment-section-input-field"
                    onChange={(event) => props.parentComponent.setState({textareaInput: event.target.value})}
                    disabled={props.parentComponent.state.disableCommentButtons}
                    required
                    value={props.parentComponent.state.textareaInput}>
                  </textarea>
                  <br/>
                  <div 
                    className="calendar-page-sidebar-error-message" 
                    style={{display: props.parentComponent.state.showErrorMessage ? "block" : "none"}}>
                      {props.parentComponent.props.postNewCommentError}
                  </div>
                  <button 
                    className="btn btn-danger calendar-page-sidebar-comment-btn"
                    onClick={ (e) => {e.preventDefault(); props.parentComponent.setState({textareaInput: "", showCommentInputFieldNr: -1, showErrorMessage: false})}}
                    style={{display: props.parentComponent.isHomePage() || props.parentComponent.isAnotherUsersCalendarFromAdminConsole() ? "span" : "none"}}
                    disabled={props.parentComponent.state.disableCommentButtons}>
                      Cancel
                  </button>
                  <input 
                    type="submit"
                    value="Send"
                    className="btn btn-primary calendar-page-sidebar-comment-btn" 
                    disabled={props.parentComponent.state.disableCommentButtons}>
                  </input>
                </div>
              </form>

              <props.parentComponent.renderCommentList vacationRequest={vacationRequest} parentComponent={props.parentComponent} />
            </ListGroupItem>
          )
        })
      )
    }
    else {
      return (<div></div>)
    }
  }


  renderCommentList(props: any) {
    if(props.parentComponent.state.comments && props.parentComponent.state.comments.length > 0) {
      return (
        <ListGroup>
          {props.parentComponent.state.comments.slice().reverse().map((comment: Comment, i: number) => {
            if(props.parentComponent.props.users && props.parentComponent.props.users.length > 0
              && comment.VacationRequestId === props.vacationRequest.Id) {
                
              return (
                <ListGroupItem key={i} className="calendar-page-sidebar-comment">
                  <ListGroupItemHeading className="calendar-page-sidebar-comment-name">
                    {props.parentComponent.props.users.filter((user: User) => user.Id === comment.UserId)[0].FirstName + " " + props.parentComponent.props.users.filter((user: User) => user.Id === comment.UserId)[0].LastName}
                    <div className="calendar-page-sidebar-comment-date">
                      {props.parentComponent.formatCommentDate(comment.Created)}
                    </div>
                  </ListGroupItemHeading>
                  <ListGroupItemText>{comment.Message}</ListGroupItemText>
                </ListGroupItem>
            )
            }
            else {
              return (<span key={i}></span>)
            }
          })}
        </ListGroup>
      )
    }
    else {
      return (<div></div>)
    }
  }

  createEvents(): any {

    let events: any = []
    let vacationRequests = this.state.user.VacationRequests
    if(this.isAnotherUsersCalendar()) {
      vacationRequests = this.props.vacationRequests.filter((vacationRequest: VacationRequest) => vacationRequest.UserId === this.state.user.Id && vacationRequest.Status === "Approved")
    }
    if(vacationRequests) {
      vacationRequests.forEach((vacationRequest: VacationRequest) => {
        let PeriodStart = this.formatStartDateForCalendar(vacationRequest.PeriodStart)
        let PeriodEnd = this.formatEndDateForCalendar(vacationRequest.PeriodEnd)
  
        let backgroundColor = null
        if(vacationRequest.Status === "Approved") {
          backgroundColor = "green"
        }
        else if(vacationRequest.Status === "Denied") {
          backgroundColor = "red"
        }
        else {
          backgroundColor = "orange"
        }
  
        events.push({
          title: 'event 1', 
          start: PeriodStart,
          end: PeriodEnd,
          rendering: 'background',
          backgroundColor: backgroundColor,
        })
      });
    }

    if(this.state.user.UserIneligiblePeriods && this.props.ineligiblePeriods) {
      this.state.user.UserIneligiblePeriods.forEach((userIneligiblePeriod: UserIneligiblePeriod) => {

        this.props.ineligiblePeriods.forEach((ineligiblePeriod: IneligiblePeriod) => {
          if(userIneligiblePeriod.IneligiblePeriodId === ineligiblePeriod.Id) {
            let PeriodStart = this.formatStartDateForCalendar(ineligiblePeriod.PeriodStart)
            let PeriodEnd = this.formatEndDateForCalendar(ineligiblePeriod.PeriodEnd)


            events.push({
              title: 'event 1', 
              start: PeriodStart,
              end:  PeriodEnd,
              rendering: 'background',
              backgroundColor: "black",
            })
          }
        })
      });
    }
    return events
  }

  createPopupIfUserClickedOnIneligiblePeriod(info: any){
    let clickedDate = new Date((info.date.getTime() + (24 * 60 * 60 * 1000)))
    let id: any = null

    if(this.state.user.UserIneligiblePeriods && this.props.ineligiblePeriods) {
      this.state.user.UserIneligiblePeriods.forEach((userIneligiblePeriod: UserIneligiblePeriod) => {

        this.props.ineligiblePeriods.forEach((ineligiblePeriod: IneligiblePeriod) => {
          if(userIneligiblePeriod.IneligiblePeriodId === ineligiblePeriod.Id) {
            let PeriodStart = this.formatStartDateForCalendar(ineligiblePeriod.PeriodStart)
            let PeriodEnd = this.formatEndDateForCalendar(ineligiblePeriod.PeriodEnd)

            if(new Date(clickedDate) >= new Date(PeriodStart) && new Date(clickedDate) <= new Date(PeriodEnd)) {
              id = userIneligiblePeriod.IneligiblePeriodId
            }
          }
        })
      });
    }

    let popup: any = document.getElementsByClassName("popuptext")[0]
    if(popup) {
      popup.parentElement.removeChild(popup.parentElement.childNodes[0]);  
    }
    else if(id && this.isAnotherUsersCalendarFromAdminConsole()){
      let span = document.createElement("span")

      span.addEventListener("click", (e: any) => {
        this.props.dispatch(deleteIneligiblePeriod(id))
        this.updateIneligiblePeriodsInCalendar()
      });
      span.setAttribute("class", `popuptext popup-show`)
      span.innerHTML = "Delete"
      info.dayEl.appendChild(span)
    }
  }

  componentDidUpdate(propsBeforeUpdate: any) {
    if (this.vacationRequestStatusUpdateWasSuccessfull(propsBeforeUpdate)) {
      this.props.dispatch(fetchUsers())
    }
    if(this.postNewCommentWasSuccessfull(propsBeforeUpdate)) {
      this.props.dispatch(fetchCurrentUser())
      this.props.dispatch(fetchUsers())
      this.setState({
        textareaInput: "",
        showCommentInputFieldNr: -1,
        disableCommentButtons: false,
        showErrorMessage: false,
      })
      if(this.state.user && !this.isAnotherUsersCalendar()) {
        this.props.dispatch(fetchAllCommentsForUser(this.state.user))
      }
    }
    if(this.postNewCommentWasNotSuccessfull(propsBeforeUpdate)) {
      this.setState({
        disableCommentButtons: false,
        showErrorMessage: true,
      })
    }
    if(this.state.user && this.props.comments.length === 0 && !this.props.fetchCommentsIsLoading && !this.isAnotherUsersCalendar()) {
      this.props.dispatch(fetchAllCommentsForUser(this.state.user))
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if(this.props !== nextProps) {
      if(this.isHomePage()) {
        this.setState({
          user: nextProps.currentUser,
          comments: nextProps.comments
        })
      }
      else {
        this.setState({
          user: nextProps.users.filter((user: User) => user.Id.toString() === this.props.match.params.id)[0],
          comments: nextProps.comments
        })
      }
      this.render()
      }
  }

  updateIneligiblePeriodsInCalendar() {
    setTimeout(() => {
      this.props.dispatch(fetchUsers())
      this.props.dispatch(fetchCurrentUser())
      this.props.dispatch(fetchIneligiblePeriods())
    }, 1000);
  }

  formatCommentDate(_date: any) {
    let date = new Date(_date)
    date.setUTCHours(date.getHours())
    return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()
  }

  _postComment(e: any, vacationRequest: VacationRequest) {
    e.preventDefault();
    this.setState({
      disableCommentButtons: true,
    })
    this.props.dispatch(postNewComment(this.props.currentUser.Id, vacationRequest.Id, this.state.textareaInput))
  }

  navigateToEditVacationRequestPage(e: any, vacationRequest: VacationRequest) {
    e.preventDefault(); 
    this.props.history.push({
      pathname: 'VacationRequest/' + vacationRequest.Id
    })
  }

  formatStartDateForCalendar(_date: Date) {
    let date = new Date(_date)
    let month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)
    let day = (date.getDate()) > 9 ? (date.getDate()) : "0" + (date.getDate())
    return date.getFullYear() + '-' + month + '-' + day
  }

  formatEndDateForCalendar(_date: Date) {
    let date = new Date(_date)
    let month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1)
    let day = (date.getDate() + 1) > 9 ? (date.getDate() + 1) : "0" + (date.getDate() + 1)
    return date.getFullYear() + '-' + month + '-' + day
  }

  vacationRequestStatusUpdateWasSuccessfull(propsBeforeUpdate: any) {
    return ((this.props.updatedVacationRequest && this.props.updatedVacationRequest !== propsBeforeUpdate.updatedVacationRequest) 
      && (this.props.updateVacationRequestError === null));
  }

  postNewCommentWasSuccessfull(propsBeforeUpdate: any) {
    return ((this.props.newComment && this.props.newComment !== propsBeforeUpdate.newComment) 
      && (this.props.postNewCommentError === null));
  }

  postNewCommentWasNotSuccessfull(propsBeforeUpdate: any) {
    return ((this.props.postNewCommentError !== propsBeforeUpdate.postNewCommentError) 
      && (this.props.postNewCommentError !== null));
  }

  acceptVactionRequest(vacationRequest: VacationRequest) {
    this.props.dispatch(updateVacationRequest(vacationRequest.Id, this.props.currentUser.Id, vacationRequest.Title, new Date(vacationRequest.PeriodStart), new Date(vacationRequest.PeriodEnd), "Approved"))
  }

  rejectVactionRequest(vacationRequest: VacationRequest) {
    this.props.dispatch(updateVacationRequest(vacationRequest.Id, this.props.currentUser.Id, vacationRequest.Title, new Date(vacationRequest.PeriodStart), new Date(vacationRequest.PeriodEnd), "Denied"))
  }

  isHomePage() {
    return this.props.location.pathname === "/"
  }
  
  isAnotherUsersCalendar() {
    return this.props.location.pathname !== "/" && !this.props.location.pathname.includes("Admin")
  }

  isAnotherUsersCalendarFromAdminConsole() {
    return this.props.location.pathname !== "/" && this.props.location.pathname.includes("Admin")
  }
  
}





function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser,
    
    users: state.userState.users,

    comments: state.commentState.comments,
    fetchCommentsIsLoading: state.commentState.fetchCommentsIsLoading,

    vacationRequests: state.vacationRequestState.vacationRequests,
    updatedVacationRequest: state.vacationRequestState.updatedVacationRequest,
    updateVacationRequestIsLoading: state.vacationRequestState.updateVacationRequestIsLoading,
    updateVacationRequestError: state.vacationRequestState.updateVacationRequestError,

    newComment: state.commentState.newComment,
    postNewCommentIsLoading: state.commentState.postNewCommentIsLoading,
    postNewCommentError: state.commentState.postNewCommentError,

    ineligiblePeriods: state.ineligiblePeriodState.ineligiblePeriods,
    newIneligiblePeriod: state.ineligiblePeriodState.newIneligiblePeriod,
    postNewIneligiblePeriodError: state.ineligiblePeriodState.postNewIneligiblePeriodError,
    deleteIneligiblePeriodIsLoading: state.ineligiblePeriodState.deleteIneligiblePeriodIsLoading,
  };
}
export default connect(mapStateToProps)(withRouter(Calendar));



