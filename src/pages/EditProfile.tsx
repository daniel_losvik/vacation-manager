import React from 'react';
import '../styles/editProfile.css';
import { Spinner, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { User } from '../types';
import { downscaleImage } from '../utils';
import { updateUser, deleteUser, fetchUsers, fetchCurrentUser, setUpdateUserWasSuccessfullNull } from '../redux/actions/userActions';


class EditProfile extends React.Component<any, any> {
  unEditedUser: any;
  constructor(props: any) {
    super(props);
    let profileOwner = this.getProfileOwnerFromURL(props)
    if(profileOwner) {
      this.state = {
        Id: profileOwner.Id,
        FirstName: profileOwner.FirstName,
        LastName: profileOwner.LastName,
        Email: profileOwner.Email,
        Password: profileOwner.Pwd,
        IsAdmin: profileOwner.IsAdmin,
        ProfilePic: profileOwner.ProfilePic,

        showEditButton: true,
        showSaveButton: false,
        disableButtons: false,
        disableInputFields: true,
    }
    // Create a copy of the unedited User
    this.unEditedUser = JSON.parse(JSON.stringify(profileOwner))
    }
  }

  componentDidMount() {
    this.props.dispatch(setUpdateUserWasSuccessfullNull())
    this.props.dispatch(fetchCurrentUser())
    this.props.dispatch(fetchUsers())
  }


  render() {
    if (this.state) {
      return (
        <div className="edit-profile-card">
          <h1 className="edit-profile-header" style={{display: this.props.currentUser.Id === this.state.Id ? "block" : "none"}}>My Profile</h1>
          <h1 className="edit-profile-header" style={{display: this.props.currentUser.Id !== this.state.Id ? "block" : "none"}}>Edit User</h1>
  
          <form onSubmit={this._updateUser.bind(this)} className="edit-profile-form">
            <div className="edit-profile-firstname-and-lastname-input-fields" >
              <input 
                  type="text" 
                  id="firstname" 
                  name="firstname" 
                  className="edit-profile-firstname-input-field" 
                  value={this.state.FirstName} placeholder="Firstname"
                  onChange={(event) => this.setState({FirstName: event.target.value})}
                  disabled={this.state.disableInputFields}
                  required>
              </input>
  
              <input 
                  type="text" 
                  id="lastname" 
                  name="lastname" 
                  className="edit-profile-lastname-input-field" 
                  value={this.state.LastName} placeholder="Lastname"
                  onChange={(event) => this.setState({LastName: event.target.value})}
                  disabled={this.state.disableInputFields}
                  required>
              </input>
            </div>
  
            <input 
              type="email" 
              id="email" 
              name="email" 
              className="edit-profile-input-field" 
              pattern=".*@tidsbankenfuncs.ninja|95danlos65@gmail.com|philipaubert@hotmail.com|ole_baadshaug@hotmail.com"
              title="Email Must End With @tidsbankenfuncs.ninja"
              value={this.state.Email} placeholder="Email" 
              onChange={(event) => this.setState({Email: event.target.value})}
              disabled={this.state.disableInputFields}
              required>
            </input>

            <input
              type="password" 
              id="password" 
              name="password" 
              className="edit-profile-input-field-password" 
              value={this.state.Email} placeholder="Password" 
              onChange={(event) => this.setState({Email: event.target.value})}
              disabled={true}
              required>
            </input>
            <button className="btn btn-secondary edit-profile-change-password-btn" onClick={(event) => this.navigateToEditPasswordPage(event)} disabled={this.state.disableInputFields || this.props.location.pathname !== "/Profile"}>Change</button>

            <div className="edit-profile-image-container">
            <img src={this.state.ProfilePic !== "" ? this.state.ProfilePic : require('../assets/profile_picture.png')} alt="Profile" className="edit-profile-img" />
              <button className="btn btn-secondary edit-profile-clear-btn" onClick={(event) => this.clearImage(event)} disabled={this.state.disableInputFields}>Clear</button>
              <label htmlFor="imageUpload" className={this.state.disableInputFields ? "btn btn-secondary edit-profile-upload-btn-disabled" : "btn btn-secondary edit-profile-upload-btn"}>Upload</label >
              <input type="file" id="imageUpload" style={{display: "none"}} onChange={(event) => this.encodeImageFileAsURL(event.target)} disabled={this.state.disableInputFields} />
            </div>
  
            <FormGroup check className="edit-profile-checkbox" style={{visibility: this.props.currentUser.IsAdmin && this.props.currentUser.Id !== this.state.Id ? "visible" : "hidden"}}>
              <Label check for="isAdmin">
              <Input 
                  type="checkbox" 
                  id="isAdmin" 
                  name="isAdmin" 
                  checked={this.state.IsAdmin}
                  onChange={(event) => this.setState({IsAdmin: event.target.checked})}
                  disabled={this.state.disableInputFields}/>
              Is Admin
              </Label>
            </FormGroup>

            

            <div className="edit-profile-success-message" style={{display: this.props.updateUserWasSuccessfull ? 'block' : 'none' }}>Updated</div>
            <div className="edit-profile-error-message">{this.props.updateUserError}</div>
            <div className="edit-profile-error-message">{this.props.deleteUserError}</div>
              
            <Spinner color="primary" className={this.props.updateUserIsLoading || this.props.deleteUserIsLoading ? "edit-profile-spinner-show" : 'edit-profile-spinner-hide' }/>
  
            <div className="edit-profile-button-container" style={{display: this.state.showEditButton ? 'block' : 'none' }}>
              <button className="btn btn-primary edit-profile-edit-btn" onClick={this.edit.bind(this)} disabled ={this.state.disableButtons}>Edit</button>
              <button className="btn btn-danger edit-profile-delete-btn" onClick={this._deleteUser.bind(this)} disabled ={this.state.disableButtons}>Delete</button>
            </div>
            <div className="edit-profile-button-container" style={{display: this.state.showSaveButton ? 'block' : 'none' }}>
              <button className="btn btn-danger edit-profile-cancel-btn" onClick={this.cancelEdit.bind(this)} disabled ={this.state.disableButtons}>Cancel</button>
              <input type="submit"  className="btn btn-success edit-profile-save-btn" value="Save" disabled ={this.state.disableButtons}></input>
            </div>
            <br/>
            <br/>
            
          </form>
        </div>
      );
    }
    else {
      return (<div></div>)
    }
  }

  componentDidUpdate(propsBeforeUpdate: any) {
    if (this.userUpdateWasSuccessfull(propsBeforeUpdate)) {
      this.props.dispatch(fetchCurrentUser())
      this.props.dispatch(fetchUsers())
      this.updatePage()
    }
    else if (this.userUpdateWasNotSuccessfull(propsBeforeUpdate)) {
      this.cancelEdit()
    }
    if(this.userDeleteWasSuccessfull(propsBeforeUpdate)) {
      this.props.history.goBack()
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if(nextProps.users !== this.props.users || nextProps.currentUser !== this.props.currentUser) {
      let profileOwner = this.getProfileOwnerFromURL(nextProps)
      if(profileOwner) {
        this.setState({
          Id: profileOwner.Id,
          FirstName: profileOwner.FirstName,
          LastName: profileOwner.LastName,
          Email: profileOwner.Email,
          Password: profileOwner.Pwd,
          IsAdmin: profileOwner.IsAdmin,
          ProfilePic: profileOwner.ProfilePic,

          showEditButton: this.state && this.state.showEditButton !== null ? this.state.showEditButton : true,
          showSaveButton: this.state && this.state.showSaveButton !== null ? this.state.showSaveButton : false,
          disableButtons: this.state && this.state.disableButtons !== null ? this.state.disableButtons : false,
          disableInputFields: this.state && this.state.disableInputFields !== null ? this.state.disableInputFields : true
      })
      // Create a copy of the unedited User
      this.unEditedUser = JSON.parse(JSON.stringify(profileOwner))
      }
    }
  }

  getProfileOwnerFromURL(props: any) {
    if(props.location.pathname === "/Profile") {
      return props.currentUser
    }
    else {
      return props.users.filter((user: User) => user.Id.toString() === props.match.params.id)[0]
    }
  }

  encodeImageFileAsURL(element: any) {
    var file = element.files[0]
    var reader = new FileReader()
    reader.onloadend = async () => {
      let result: any = reader.result
      let imageType = result.split(':')[1].split(';')[0]
      let image = await downscaleImage(result, imageType, 200, 0.7)

      this.setState({
        ProfilePic: image,
      })
      document.getElementsByClassName("edit-profile-img")[0].setAttribute("src", image)
    }
    if(file) {
      reader.readAsDataURL(file)
    }
  }

  clearImage(e: any) {
    e.preventDefault()
    this.setState({
      ProfilePic: "",
    })
    let form: any = document.getElementsByClassName("edit-profile-form")[0]
    form.reset()
  }

  _updateUser(e: any) {
    e.preventDefault()
    this.disableButtonsAndInputFields()
    this.props.dispatch(updateUser(this.state.Id, this.state.FirstName, this.state.LastName, this.state.Email, this.state.IsAdmin, this.state.ProfilePic));
  }

  _deleteUser(e: any) {
    e.preventDefault()
    if(window.confirm("Are You Sure You Want To Delete The User. This Action Cannot Be Undone")) {
      this.disableButtonsAndInputFields()
      this.props.dispatch(deleteUser(this.state.Id))
    }
  }

  disableButtonsAndInputFields() {
    this.setState({
      disableButtons: true,
      disableInputFields: true,
    })
  }

  edit(e: any) {
    e.preventDefault();
    this.setState({
      showEditButton: false,
      showSaveButton: true,
      disableInputFields: false
    })
    this.props.dispatch(setUpdateUserWasSuccessfullNull())
  }

  cancelEdit(e: any = new Event("")) {
    e.preventDefault();
    this.setState({
      id: this.unEditedUser.id,
      FirstName: this.unEditedUser.FirstName,
      LastName: this.unEditedUser.LastName,
      Email: this.unEditedUser.Email,
      IsAdmin: this.unEditedUser.IsAdmin,
      ProfilePic: this.unEditedUser.ProfilePic,

      showEditButton: true,
      showSaveButton: false,
      disableButtons: false,
      disableInputFields: true
    })
    let form: any = document.getElementsByClassName("edit-profile-form")[0]
    form.reset()
  }
  
  userUpdateWasSuccessfull(propsBeforeUpdate: any) {
    return ((this.props.updateUserWasSuccessfull && this.props.updateUserWasSuccessfull !== propsBeforeUpdate.updateUserWasSuccessfull) 
      && (this.props.updateUserError === null));
  }

  userUpdateWasNotSuccessfull(propsBeforeUpdate: any) {
    return ((this.props.updateUserError !== propsBeforeUpdate.updateUserError) 
      && (this.props.updateUserError !== null));
  }

  userDeleteWasSuccessfull(propsBeforeUpdate: any) {
    return this.props.deleteUserWasSuccessfull && this.props.deleteUserWasSuccessfull !== propsBeforeUpdate.deleteUserWasSuccessfull
  }

  updatePage() {
    this.unEditedUser = JSON.parse(JSON.stringify(this.props.updatedUser))
    this.setState({
      showEditButton: true,
      showSaveButton: false,
      disableButtons: false,
      disableInputFields: true
    })
  }

  navigateToEditPasswordPage(e: any) {
    e.preventDefault(); 
    this.props.history.push({
      pathname: '/Profile/ChangePassword'
    })
  }
}





function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser,

    users: state.userState.users,

    updatedUser: state.userState.updatedUser,
    updateUserIsLoading: state.userState.updateUserIsLoading,
    updateUserError: state.userState.updateUserError,
    updateUserWasSuccessfull: state.userState.updateUserWasSuccessfull,
    
    deleteUserIsLoading: state.userState.deleteUserIsLoading,
    deleteUserError: state.userState.deleteUserError,
    deleteUserWasSuccessfull: state.userState.deleteUserWasSuccessfull,
  };
}
export default connect(mapStateToProps)(withRouter(EditProfile));



