import React from 'react';
import '../styles/newVacationRequest.css';
import { Spinner, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { UserIneligiblePeriod, IneligiblePeriod } from '../types';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { postNewVacationRequest, setVacationRequestError } from '../redux/actions/vacationRequestActions';
import { fetchCurrentUser } from '../redux/actions/userActions';
import { fetchIneligiblePeriods } from '../redux/actions/ineligiblePeriodAction';


class NewVacationRequest extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
        PeriodStart: null,
        PeriodEnd: null,
        Title: "",
        disableButton: true,
    };
  }

  componentDidMount() {
    this.props.dispatch(fetchCurrentUser())
    this.props.dispatch(fetchIneligiblePeriods())
    this.props.dispatch(setVacationRequestError(""))
  }

  render() {
    return (
      <div className="new-vacation-request-card">
        <h1 className="new-vacation-request-header">New Vacation Request</h1>

        <form onSubmit={this._postNewVacationRequest.bind(this)} className="new-vacation-request-form">

          <Label className="new-vacation-request-label-from">From</Label>
          <DatePicker 
            className="new-vacation-request-datepicker-from" 
            selected={this.state.PeriodStart} 
            onChange={(date: Date) =>{this.setState({PeriodStart: this.formatDate(date)}, () => this.validateDates())}} />
          
          <Label className="new-vacation-request-label-to">To</Label>
          <DatePicker 
            className="new-vacation-request-datepicker-to" 
            selected={this.state.PeriodEnd} 
            onChange={(date: Date) =>{this.setState({PeriodEnd: this.formatDate(date)}, () => this.validateDates())}} />

          <Input 
            type="text" 
            name="title" 
            id="title" 
            className="new-vacation-request-input-field" 
            placeholder="Title" 
            value={this.state.Title}
            onChange={(event) => this.setState({Title: event.target.value})}
            required/>

          <div className="new-vacation-request-error-message">{this.props.postNewVacationRequestError}</div>
            
          <Spinner color="primary" className={this.props.postNewVacationRequestIsLoading ? "new-vacation-request-spinner-show" : 'new-vacation-request-spinner-hide' }/>
          
          <input type="submit"  className="btn btn-primary new-vacation-request-submit-btn" value="Send Request" disabled={this.state.disableButton}></input>
          
        </form>
      </div>
    );
  }

  componentDidUpdate(propsBeforeUpdate: any) {
    if (this.postNewVacationRequestWasSuccessfull(propsBeforeUpdate)) {
      this.props.history.push("/")
    }
  }

  _postNewVacationRequest(e: any) {
    e.preventDefault();
    this.props.dispatch(fetchCurrentUser())
    this.props.dispatch(postNewVacationRequest(this.props.currentUser.Id, this.state.Title, this.state.PeriodStart, this.state.PeriodEnd));
  }

  postNewVacationRequestWasSuccessfull(propsBeforeUpdate: any) {
    return ((this.props.newVacationRequest && this.props.newVacationRequest !== propsBeforeUpdate.newVacationRequest) 
      && (this.props.postNewVacationRequestError === null));
  }

  formatDate(date: Date) {
    return new Date(date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate())
  }

  enableButton() {
    this.setState({
      disableButton: false,
    })
  }
  
  disableButton() {
    this.setState({
      disableButton: true,
    })
  }

  validateDates() {
    if(this.state.PeriodStart && this.state.PeriodEnd) {
      this.enableButton()
      this.props.dispatch(setVacationRequestError(""))

      if(this.state.PeriodStart > this.state.PeriodEnd) {
        this.props.dispatch(setVacationRequestError("Invalid Dates"))
        this.disableButton()
      }
      else if(this.props.currentUser.UserIneligiblePeriods && this.props.ineligiblePeriods) {
        this.props.currentUser.UserIneligiblePeriods.forEach((userIneligiblePeriod: UserIneligiblePeriod) => {

          this.props.ineligiblePeriods.forEach((ineligiblePeriod: IneligiblePeriod) => {
            if(userIneligiblePeriod.IneligiblePeriodId === ineligiblePeriod.Id) {

              if((this.state.PeriodStart <= new Date(ineligiblePeriod.PeriodStart) &&  new Date(ineligiblePeriod.PeriodStart) <= this.state.PeriodEnd)
                || (this.state.PeriodStart <= new Date(ineligiblePeriod.PeriodEnd) &&  new Date(ineligiblePeriod.PeriodEnd) <= this.state.PeriodEnd)
                || (this.state.PeriodStart >= new Date(ineligiblePeriod.PeriodStart) &&  new Date(ineligiblePeriod.PeriodEnd) >= this.state.PeriodEnd))
              {
              this.props.dispatch(setVacationRequestError("Invalid Dates"))
              this.disableButton()
              }
            }
          })
        })
      }
    }
  }
}





function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser,

    ineligiblePeriods: state.ineligiblePeriodState.ineligiblePeriods,

    newVacationRequest: state.vacationRequestState.newVacationRequest,
    postNewVacationRequestIsLoading: state.vacationRequestState.postNewVacationRequestIsLoading,
    postNewVacationRequestError: state.vacationRequestState.postNewVacationRequestError,
  };
}
export default connect(mapStateToProps)(withRouter(NewVacationRequest));


