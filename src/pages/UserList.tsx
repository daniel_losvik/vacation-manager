import React from 'react';
import '../styles/userList.css';
import { Table, Spinner } from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { User, VacationRequest } from '../types';
import { fetchUsers } from '../redux/actions/userActions';


class UserList extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
        searchKey: ""
    };
  }

  componentDidMount() {
       this.props.dispatch(fetchUsers())
  }

  render() {
    return (
      <div>
           <input 
                style={{position:"absolute", left:"85%", top:"12%", width:"14%"}}
                type="text" 
                name="searchKey" 
                className="user-list-search-field" 
                placeholder="Search" 
                value={this.state.searchKey}
                onChange={ (event) => {this.setState({searchKey: event.target.value})}} />
        <Table hover>
        <thead>
            <tr>
            <th></th>
            <th>First Name</th>
            <th>Last Name</th>
            <th><div style={{display: this.props.location.pathname === "/Admin/Users" ? "block" : "none"}}>Email</div></th>
            <th>#</th>
            </tr>
        </thead>
        <tbody>
            {this.sortUsersByLastnameAndPendingRequests(this.props.users)
              .filter((user: User) => {return this.userNameOrEmailContainsSearchKey(user)})
                .map((user: User, i: number) => {
                    return (
                        <tr key={i} className="user-list-table-row" 
                          onClick={ (e) => {
                            this.navigateToCalendarPage(e, user)
                            }}>
                            <td><img src={user.ProfilePic !== "" ? user.ProfilePic : require('../assets/profile_picture.png')} alt="Profile" className="user-list-profile-picture-img"/></td>
                            <td>{user.FirstName}</td>
                            <td>{user.LastName}
                            <svg 
                                style={{position:"absolute", 
                                color:"orange", 
                                transform:"scale(1.5)", 
                                display: this.userHasPendingRequests(user) && this.props.location.pathname === "/Admin/Users" ? 'inline-block' : 'none'  }} 
                                className="bi bi-exclamation exclamation-mark" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.002 11a1 1 0 112 0 1 1 0 01-2 0zM7.1 4.995a.905.905 0 111.8 0l-.35 3.507a.552.552 0 01-1.1 0L7.1 4.995z"/>
                              </svg>
                            </td>
                            <td><div style={{display: this.props.location.pathname === "/Admin/Users" ? "block" : "none"}}>{user.Email.toString().length > 25 ? user.Email.toString().substring(0, 22) + "..." : user.Email}</div></td>
                            <td><img 
                              src={require('../assets/edit.png')} 
                              alt="Edit Icon" 
                              className="user-list-edit-icon" 
                              style={{display: this.props.location.pathname === "/Admin/Users" ? "block" : "none"}}
                              onClick={ (e) => {
                                this.navigateToEditProfilePage(e, user)
                              }}/></td>
                        </tr>
                    )
        })}
        </tbody>
        </Table>
        <Spinner color="primary" className={this.props.users.length === 0 && this.props.fetchUsersIsLoading ? "user-list-spinner-show" : 'user-list-spinner-hide' }/>
      </div>
    );
  }

  sortUsersByLastnameAndPendingRequests(users: User[]) {
    if(this.props.location.pathname.includes("Admin")) {
      let pendingUsers = users.filter((user: User) => {
        return this.userHasPendingRequests(user)
      }).filter((user:User) =>{return user && user.LastName}).sort((a: User, b: User) => {return a.LastName.localeCompare(b.LastName)})

      let notPendingUsers = users.filter((user: User) => {
        return !this.userHasPendingRequests(user)
      }).filter((user:User) =>{return user && user.LastName}).sort((a: User, b: User) => {return a.LastName.localeCompare(b.LastName)})

      return pendingUsers.concat(notPendingUsers)
    }
    else {
      return users.filter((user:User) =>{return user && user.LastName}).sort((a: User, b: User) => {return a.LastName.localeCompare(b.LastName)})
    }
  }

  navigateToCalendarPage(e: any, user: User) {
    e.preventDefault(); 
    let path = 'User/' + user.Id + '/Calendar'
    if (this.props.currentUser.Id === user.Id) {
      path = '/'
    }
    this.props.history.push({
      pathname: path
    })
  }

  navigateToEditProfilePage(e: any, user: User) {
    e.stopPropagation(); 
    e.preventDefault(); 
    let path = 'User/' + user.Id + '/Profile'
    if (this.props.currentUser.Id === user.Id) {
      path = '/Profile'
    }
    this.props.history.push({
      pathname: path
    })
  }

  userNameOrEmailContainsSearchKey(user: User) {
    return (user && user.FirstName && user.FirstName.toLowerCase().includes(this.state.searchKey.toLowerCase()))
    || (user && user.FirstName && user.LastName.toLowerCase().includes(this.state.searchKey.toLowerCase()))
    || (user && user.FirstName && user.Email.toLowerCase().includes(this.state.searchKey.toLowerCase()))
  }

  userHasPendingRequests(user: User) {
    let hasPendingRequests = false
    if(user.VacationRequests) {
        user.VacationRequests.forEach((vacationRequest: VacationRequest) => {
          if(vacationRequest.Status === "Pending") {
            hasPendingRequests = true
          }
        })
      }
    return hasPendingRequests
  }
}





function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser,

    users: state.userState.users,
    fetchUsersIsLoading: state.userState.fetchUsersIsLoading
  };
}
export default connect(mapStateToProps)(withRouter(UserList));


