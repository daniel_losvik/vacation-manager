import React from 'react';
import '../../styles/admin/adminAddUser.css';
import { Spinner, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { postNewUser, SetNewUserPasswordNull } from '../../redux/actions/userActions';


class AdminAddUser extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
        FirstName: "",
        LastName: "",
        Email: "",
        IsAdmin: false,
    };
    this.props.dispatch(SetNewUserPasswordNull())
  }


  render() {
    return (
      <div className="admin-add-user-card">
        <h1 className="admin-add-user-header">Add New User</h1>

        <form onSubmit={this.postNewUser.bind(this)} className="admin-add-user-form">
          <div className="admin-add-user-firstname-and-lastname-input-fields" >
            <input 
                type="text" 
                id="firstname" 
                name="firstname" 
                className="admin-add-user-firstname-input-field" 
                placeholder="Firstname"
                value={this.state.FirstName}
                onChange={(event) => this.setState({FirstName: event.target.value})}
                required>
            </input>

            <input 
                type="text" 
                id="lastname" 
                name="lastname" 
                className="admin-add-user-lastname-input-field" 
                placeholder="Lastname"
                value={this.state.LastName}
                onChange={(event) => this.setState({LastName: event.target.value})}
                required>
            </input>
          </div>

          <input 
            type="text" 
            id="email" 
            name="email" 
            className="admin-add-user-input-field" 
            placeholder="Email" 
            value={this.state.Email}
            onChange={(event) => this.setState({Email: event.target.value})}
            required>
          </input>
          <div style={{position:"absolute", left:"47%", top:"37%", fontSize:"18px"}}>@tidsbankenfuncs.ninja</div>

          <FormGroup check className="admin-add-user-checkbox">
            <Label check for="isAdmin">
            <Input 
                type="checkbox" 
                id="isAdmin" 
                name="isAdmin" 
                checked={this.state.IsAdmin}
                onChange={(event) => this.setState({IsAdmin: event.target.checked})}/>
            Is Admin
            </Label>
        </FormGroup>

          <div className="admin-add-user-error-message">{this.props.postNewUserError}</div>
          <div className="admin-add-user-password" style={{display: this.props.password ? "block" : "none"}}>
            <span style={{color:"green"}}>Success! </span><span>New User Password:</span>
            <div>{this.props.password}</div>
          </div>
            
          <Spinner color="primary" className={this.props.postNewUserIsLoading ? "admin-add-user-spinner-show" : 'admin-add-user-spinner-hide' }/>
          
          <input type="submit" className="btn btn-primary admin-add-user-btn" value="Create" style={{display: !this.props.password ? "block" : "none"}}></input>

          <button className="btn btn-primary admin-back-btn" style={{display: this.props.password ? "block" : "none"}} onClick={() => this.goBack()}>Back</button>
          
        </form>
      </div>
    );
  }

  goBack() {
    this.props.history.push("/")
  }

  postNewUser(e: any) {
    e.preventDefault();
    if(!this.props.password) {
      this.props.dispatch(postNewUser(this.state.FirstName, this.state.LastName, this.state.Email + "@tidsbankenfuncs.ninja", this.state.IsAdmin));
    }
  }

  postNewUserWasSuccessfull(propsBeforeUpdate: any) {
    return ((this.props.password && this.props.password !== propsBeforeUpdate.password) 
      && (this.props.postNewUserError === null));
  }
}





function mapStateToProps(state: any) {
  return {
    password: state.userState.password,
    postNewUserIsLoading: state.userState.postNewUserIsLoading,
    postNewUserError: state.userState.postNewUserError,
  };
}
export default connect(mapStateToProps)(withRouter(AdminAddUser));


