import React from 'react';
import '../../styles/admin/adminDashboard.css';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { User, VacationRequest } from '../../types';


class AdminDashboard extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }


  render() {
    return (
      <div className="admin-dashboard-container">
        <div className="admin-dashboard-header">Admin Dashboard</div>
        <div className="admin-dashboard-card" onClick={ (e) => {this.props.history.push("Users")}}>
            <div className="admin-dashboard-card-text-two-lines">
                Administrate 
                <svg 
                  style={{position:"absolute", color:"orange", transform:"scale(1.5)", display: anyUserHasPendingRequests(this.props.users) ? 'inline-block' : 'none'}} 
                    className="bi bi-exclamation exclamation-mark" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path d="M7.002 11a1 1 0 112 0 1 1 0 01-2 0zM7.1 4.995a.905.905 0 111.8 0l-.35 3.507a.552.552 0 01-1.1 0L7.1 4.995z"/>
                </svg>
                <br/>
                Users
            </div>
        </div>
        <div className="admin-dashboard-card" onClick={ (e) => {this.props.history.push("AddUser")}}>
            <div className="admin-dashboard-card-text">
                Add New User
            </div>
        </div>
      </div>
    );
  }
}


export function anyUserHasPendingRequests(users: User[]) {
  let hasPendingRequests = false
  if(users) {
    users.forEach((user: User) => {
      if(user.VacationRequests) {
        user.VacationRequests.forEach((vacationRequest: VacationRequest) => {
          if(vacationRequest.Status === "Pending") {
            hasPendingRequests = true
          }
        })
      }
    });
  }
  return hasPendingRequests
}




function mapStateToProps(state: any) {
  return {
    users: state.userState.users,
  };
}
export default connect(mapStateToProps)(withRouter(AdminDashboard));


