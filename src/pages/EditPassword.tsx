import React from 'react';
import '../styles/editPassword.css';
import { Spinner } from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { User } from '../types';
import { fetchCurrentUser, setUpdateUserWasSuccessfullNull, updateUserPassword } from '../redux/actions/userActions';


class EditPassword extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    let profileOwner = this.getProfileOwnerFromURL(props)
    if(profileOwner) {
      this.state = {
        Id: profileOwner.Id,
        PasswordOld: "",
        PasswordNew: "",
        PasswordNewRepeat: "",
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(setUpdateUserWasSuccessfullNull())
    this.props.dispatch(fetchCurrentUser())
  }


  render() {
    if (this.state) {
      return (
        <div className="edit-password-card">
          <h1 className="edit-password-header">Change Password</h1>
  
          <form onSubmit={this._updateUserPassword.bind(this)} className="edit-password-form">

            <input
              type="password" 
              id="password-old" 
              name="password-old" 
              className="edit-password-input-field-password" 
              value={this.state.PasswordOld} placeholder="Old Password" 
              onChange={(event) => this.setState({PasswordOld: event.target.value})}
              disabled={this.props.updateUserPasswordIsLoading}
              required>
            </input>

            <input
              type="password" 
              id="password-new" 
              name="password-new" 
              className="edit-password-input-field-password" 
              value={this.state.PasswordNew} placeholder="New Password" 
              onChange={(event) => this.setState({PasswordNew: event.target.value})}
              disabled={this.props.updateUserPasswordIsLoading}
              required>
            </input>

            <input
              type="password" 
              id="password-new-repeat" 
              name="password-new-repeat" 
              className="edit-password-input-field-password" 
              value={this.state.PasswordNewRepeat} placeholder="Repeat New Password" 
              onChange={(event) => this.setState({PasswordNewRepeat: event.target.value})}
              disabled={this.props.updateUserPasswordIsLoading}
              required>
            </input>

            <div className="edit-password-info-message">
            Must Be Between 8 to 64 characters <br/>
            and include 3 out of the following 4: <br/>
            - lowercase <br/>
            - Uppercase <br/>
            - Numbers <br/>
            - Symbols
            </div>

            <div className="edit-password-success-message" style={{display: this.props.updateUserPasswordWasSuccessfull ? 'block' : 'none' }}>Updated</div>
            <div className="edit-password-error-message">{this.props.updateUserPasswordError}</div>
              
            <Spinner color="primary" className={this.props.updateUserPasswordIsLoading ? "edit-password-spinner-show" : 'edit-password-spinner-hide' }/>
  
            <div className="edit-password-button-container">
              <button className="btn btn-secondary edit-password-cancel-btn" onClick={this.cancelEdit.bind(this)} disabled ={this.props.updateUserPasswordIsLoading }>Back</button>
              <input type="submit" className="btn btn-success edit-password-save-btn" value="Save" disabled ={this.props.updateUserPasswordIsLoading }></input>
            </div>
            <br/>
            <br/>
            
          </form>
        </div>
      );
    }
    else {
      return (<div></div>)
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if(nextProps.users !== this.props.users || nextProps.currentUser !== this.props.currentUser) {
      let profileOwner = this.getProfileOwnerFromURL(nextProps)
      if(profileOwner) {
        this.setState({
          Id: profileOwner.Id,
          PasswordOld: "",
          PasswordNew: "",
          PasswordNewRepeat: "",
        })
      }
    }
  }

  getProfileOwnerFromURL(props: any) {
    if(props.location.pathname === "/Profile/ChangePassword") {
      return props.currentUser
    }
    else {
      return props.users.filter((user: User) => user.Id.toString() === props.match.params.id)[0]
    }
  }

  _updateUserPassword(e: any) {
    e.preventDefault()
    this.props.dispatch(updateUserPassword(this.state.Id, this.state.PasswordOld, this.state.PasswordNew));
  }

  cancelEdit(e: any = new Event("")) {
    e.preventDefault()
    this.props.history.goBack()
  }
}





function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser,

    users: state.userState.users,

    updateUserPasswordIsLoading: state.userState.updateUserPasswordIsLoading,
    updateUserPasswordError: state.userState.updateUserPasswordError,
    updateUserPasswordWasSuccessfull: state.userState.updateUserPasswordWasSuccessfull,
  };
}
export default connect(mapStateToProps)(withRouter(EditPassword));



