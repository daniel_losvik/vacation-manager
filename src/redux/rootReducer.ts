import { combineReducers } from 'redux';
import { userReducer } from '../redux/reducers/userReducer';
import { vacationRequestReducer } from '../redux/reducers/vacationRequestReducer';
import { commentReducer } from './reducers/commentReducer';
import { ineligiblePeriodReducer } from './reducers/IneligiblePeriodReducer';

export const rootReducer = combineReducers({
    userState: userReducer,
    vacationRequestState: vacationRequestReducer,
    commentState: commentReducer,
    ineligiblePeriodState: ineligiblePeriodReducer,
});
export type RootState = ReturnType<typeof rootReducer>;