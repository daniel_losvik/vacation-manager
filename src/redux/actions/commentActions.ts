import { logout, setLoginError } from "./userActions";


export function postNewComment(UserId: number, VacationRequestId: number, Message: string) {
    return (dispatch: any, getState: any) => {
      dispatch({
        type: 'POST_NEW_COMMENT_BEGIN'
      });      
      

      return fetch('https://tidsbankenfuncs.azurewebsites.net/api/request/' + VacationRequestId + '/comment', {
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${getState().userState.currentUser.token}`,

          "UserId": UserId.toString(),
          "VacationRequestId": VacationRequestId.toString(),
          "Message": Message,
          }
        })
        .then(response => {
          if(response.ok) {
            response.json()
              .then(body => { 
                dispatch({
                  type: 'POST_NEW_COMMENT_SUCCESS',
                  newComment: JSON.parse(body)
                });
              })
          }
          else if(response.status === 401) {
            dispatch(setLoginError("Session Expired"))
            dispatch(logout())
          }
          else {
            dispatch({
              type: 'POST_NEW_COMMENT_FAILURE',
              error: "An Error Occured"
            });
          }
        })
        .catch((error: any) => {
          console.log(error)
        })
    }
}


export function fetchAllCommentsForUser(user: any) {
  return (dispatch: any, getState: any) => {
    dispatch(setFetchCommentIsLoading(true))
    user.VacationRequests.forEach((vacationRequest: any) => {
        fetch('https://tidsbankenfuncs.azurewebsites.net/api/request/' + vacationRequest.Id + '/comment', {
          method: 'GET',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${getState().userState.currentUser.token}`,
          }
        })
        .then(response => {
          if(response.ok && response.statusText !== "No Content") {
            response.json()
              .then(body => { 

                body = JSON.parse(body)
                let comments: any = getState().commentState.comments

                body.forEach((body_comment: any) => {
                  let exists = false
                  getState().commentState.comments.forEach((comment: any) => {
                    if(comment.UserId === body_comment.UserId && comment.VacationRequestId === body_comment.VacationRequestId) {
                      exists = true
                    }
                  });
                  if(!exists) {
                    comments.push(body_comment)
                  }
                });

                dispatch({
                  type: 'SET_COMMENTS',
                  comments: comments
                });
              })
          }
          else if(response.status === 401) {
            dispatch(setLoginError("Session Expired"))
            dispatch(logout())
          }

        })
        .catch((error: any) => {
          console.log(error)
        })
    });  
  }
}


export function setFetchCommentIsLoading(fetchCommentsIsLoading: boolean) {
  return (dispatch: any) => {
    dispatch({
      type: 'SET_FETCH_COMMENTS_IS_LOADING',
      fetchCommentsIsLoading: fetchCommentsIsLoading
    });  
  }
}