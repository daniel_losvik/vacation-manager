import * as Msal from 'msal'


export function requestLogin() {
    return (dispatch: any) => {
      let scope = { scopes: ["https://tidsbankenfuncs.azurewebsites.net/user_impersonation"] }
      
      let myMsalObj: any = new Msal.UserAgentApplication(
        {
          auth: {
            clientId: "f947cb19-3e80-47bf-ad66-fcb270b9b7db",
            authority: 'https://login.microsoftonline.com/668501c6-8894-4b5c-9039-295357a3a5e0/',
            validateAuthority: false,
            //redirectUri: "http://localhost:3000"
            redirectUri: "https://www.tidsbankenfuncs.ninja"
          },
          cache: {
            cacheLocation: "localStorage",
            storeAuthStateInCookie: true
          }
        }
      )

      myMsalObj.loginPopup(scope)
      .then(() => {
        
        myMsalObj.acquireTokenSilent(scope)
        .then( (token: { accessToken: any; }) => {
          myMsalObj.acquireTokenSilent({ scopes: ["https://graph.microsoft.com/RoleManagement.ReadWrite.Directory", "https://graph.microsoft.com/AppRoleAssignment.ReadWrite.All"]})
            .then( (token_2: { accessToken: any; }) => {
              dispatch({
                type: 'REQUEST_LOGIN_BEGIN'
              });
              
              fetch('https://tidsbankenfuncs.azurewebsites.net/api/user', {
                method: 'GET',
                headers: {
                  'Content-Type': 'application/json',
                  "Authorization": `Bearer ${token.accessToken}`
                },
              })
              .then(response => {
                if(response.ok) {
                  response.json()
                    .then(body => { 
                      let currentUser = body
                      currentUser.token = token.accessToken
                      currentUser.token_2 = token_2.accessToken
                      dispatch({
                        type: 'REQUEST_LOGIN_SUCCESS',
                        currentUser: currentUser
                      });
                    })
                }
                else {
                  dispatch({
                    type: 'REQUEST_LOGIN_FAILURE',
                    error: "An Error Occured"
                  });
                }
              })
            })
        })
      })
      .catch((error: any) => {
        console.log(error)
        dispatch({
          type: 'REQUEST_LOGIN_FAILURE',
          error: "An Error Occured"
        });
      })
    }
  }


export function logout() {
  return (dispatch: any) => {
    sessionStorage.removeItem("currentUser");
    dispatch({
      type: 'LOGOUT'
    });
  }
}


export function setLoginError(error: string) {
  return (dispatch: any) => {
    dispatch({
      type: 'REQUEST_LOGIN_FAILURE',
      error: error
    });
  }
}


export function setCurrentUser(currentUser: any) {
  return (dispatch: any) => {
    dispatch({
      type: 'SET_CURRENT_USER',
      currentUser : currentUser
    });
  }
}


export function fetchCurrentUser() {
  return (dispatch: any, getState: any) => {
    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/user', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${getState().userState.currentUser.token}`
        },
      })
      .then(response => {
        if(response.ok) {
          response.json()
            .then(body => { 
              let currentUser = body
              currentUser.token = getState().userState.currentUser.token
              currentUser.token_2 = getState().userState.currentUser.token_2
              dispatch({
                type: 'SET_CURRENT_USER',
                currentUser: currentUser
              });
            })
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function fetchUsers() {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: 'FETCH_USERS_BEGIN'
    });
    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/user/getall', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${getState().userState.currentUser.token}`
        },
      })
      .then(response => {
        if(response.ok) {
          response.json()
            .then(body => { 
              dispatch({
                type: 'FETCH_USERS_SUCCESS',
                users: body
              });
            })
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'FETCH_USERS_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function postNewUser(FirstName: string, LastName: string, Email: string, IsAdmin: boolean) {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: 'POST_NEW_USER_BEGIN'
    });

    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/user', {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`,
        "GraphToken": `${getState().userState.currentUser.token_2}`,

        "FirstName": FirstName,
        "LastName": LastName,
        "Email": Email,
        "IsAdmin": "false",
        "ProfilePic": "",
        }
      })
      .then(response => {
        if(response.ok) {
          response.json()
            .then(body => { 
              dispatch({
                type: 'POST_NEW_USER_SUCCESS',
                password: JSON.parse(body).Pwd
              });
              if(IsAdmin) {
                dispatch(updateUser(JSON.parse(body).Id, FirstName, LastName, Email, true, ""))
              }
            })
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'POST_NEW_USER_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function updateUser(Id: number, FirstName: string, LastName: string, Email: string, IsAdmin: boolean, ProfilePic: string) {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: 'UPDATE_USER_BEGIN'
    });
    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/user/' + Id, {
        method: 'PATCH',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`,
        "GraphToken": `${getState().userState.currentUser.token_2}`,

        "FirstName": FirstName,
        "LastName": LastName,
        "Email": Email,
        "IsAdmin": IsAdmin.toString(),
        "ProfilePic": ProfilePic,
        }
      })
      .then(response => {
        if(response.ok) {
          dispatch({
            type: 'UPDATE_USER_SUCCESS',
            updatedUser: null,
            updateUserWasSuccessfull: true
          });
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'UPDATE_USER_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function updateUserPassword(UserId: number, PasswordOld: string, PasswordNew: string) {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: 'UPDATE_USER_PASSWORD_BEGIN'
    });
    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/user/' + UserId + '/update_password/', {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`,
        "GraphToken": `${getState().userState.currentUser.token_2}`,
        },
        body: JSON.stringify({
          "Current": PasswordOld,
          "newPass": PasswordNew,
        })
      })
      .then(response => {
        if(response.ok) {
          dispatch({
            type: 'UPDATE_USER_PASSWORD_SUCCESS',
            updateUserPasswordWasSuccessfull: true
          });
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'UPDATE_USER_PASSWORD_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function deleteUser(Id: number) {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: 'DELETE_USER_BEGIN'
    });
    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/user/' + Id, {
        method: 'DELETE',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`,
        "GraphToken": `${getState().userState.currentUser.token_2}`,
        },
      })
      .then(response => {
        if(response.ok) {
          response.json()
            .then(body => { 
              dispatch({
                type: 'DELETE_USER_SUCCESS',
              });
            })
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'DELETE_USER_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function SetNewUserPasswordNull() {
  return (dispatch: any) => {
    dispatch({
      type: 'POST_NEW_USER_SUCCESS',
      password: null
    });
  }
}


export function setUpdateUserWasSuccessfullNull() {
  return (dispatch: any) => {
    dispatch({
      type: 'UPDATE_USER_SUCCESS',
      updatedUser: null,
      updateUserWasSuccessfull: false
    });
    dispatch({
      type: 'UPDATE_USER_PASSWORD_SUCCESS',
      updatedUser: null,
      updateUserPasswordWasSuccessfull: false
    });
  }
}