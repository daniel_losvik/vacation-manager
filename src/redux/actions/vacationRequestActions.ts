import { logout, setLoginError } from "./userActions";


export function fetchVacationRequests() {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: 'FETCH_VACATION_REQUESTS_BEGIN'
    });      

    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/request', {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`,
        }
      })
      .then(response => {
        if(response.ok) {
          response.json()
            .then(body => { 
              dispatch({
                type: 'FETCH_VACATION_REQUESTS_SUCCESS',
                vacationRequests: JSON.parse(body)
              });
            })
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function postNewVacationRequest(UserId: number, Title: string, PeriodStart: Date, PeriodEnd: Date) {
    return (dispatch: any, getState: any) => {
      dispatch({
        type: 'POST_NEW_VACATION_REQUEST_BEGIN'
      });      

      let PeriodStartMonth = (PeriodStart.getMonth() + 1) > 9 ? (PeriodStart.getMonth() + 1) : "0" + (PeriodStart.getMonth() + 1)
      let PeriodEndMonth = (PeriodEnd.getMonth() + 1) > 9 ? (PeriodEnd.getMonth() + 1) : "0" + (PeriodEnd.getMonth() + 1)
      let PeriodStartDay = (PeriodStart.getDate()) > 9 ? (PeriodStart.getDate()) : "0" + (PeriodStart.getDate())
      let PeriodEndDay = (PeriodEnd.getDate()) > 9 ? (PeriodEnd.getDate()) : "0" + (PeriodEnd.getDate())


      return fetch('https://tidsbankenfuncs.azurewebsites.net/api/request', {
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${getState().userState.currentUser.token}`,

          "UserId": UserId.toString(),
          "Title": Title,
          "PeriodStart": PeriodStart.getFullYear() + "-" + PeriodStartMonth + "-" + PeriodStartDay,
          "PeriodEnd": PeriodEnd.getFullYear() + "-" + PeriodEndMonth + "-" + PeriodEndDay
          }
        })
        .then(response => {
          if(response.ok) {
            response.json()
              .then(body => { 
                dispatch({
                  type: 'POST_NEW_VACATION_REQUEST_SUCCESS',
                  newVacationRequest: JSON.parse(body)
                });
              })
          }
          else if(response.status === 401) {
            dispatch(setLoginError("Session Expired"))
            dispatch(logout())
          }
          else {
            dispatch({
              type: 'POST_NEW_VACATION_REQUEST_FAILURE',
              error: "An Error Occured"
            });
          }
        })
        .catch((error: any) => {
          console.log(error)
        })
    }
  }


export function updateVacationRequest(Id: number, UserId: number, Title: string, PeriodStart: Date, PeriodEnd: Date, Status: string) {
    return (dispatch: any, getState: any) => {
      dispatch({
        type: 'UPDATE_VACATION_REQUEST_BEGIN'
      });

      let PeriodStartMonth = (PeriodStart.getMonth() + 1) > 9 ? (PeriodStart.getMonth() + 1) : "0" + (PeriodStart.getMonth() + 1)
      let PeriodEndMonth = (PeriodEnd.getMonth() + 1) > 9 ? (PeriodEnd.getMonth() + 1) : "0" + (PeriodEnd.getMonth() + 1)
      let PeriodStartDay = (PeriodStart.getDate()) > 9 ? (PeriodStart.getDate()) : "0" + (PeriodStart.getDate())
      let PeriodEndDay = (PeriodEnd.getDate()) > 9 ? (PeriodEnd.getDate()) : "0" + (PeriodEnd.getDate())

      
      return fetch('https://tidsbankenfuncs.azurewebsites.net/api/request/' + Id, {
          method: 'PATCH',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${getState().userState.currentUser.token}`,

          "UserId": UserId.toString(),
          "Title": Title,
          "PeriodStart": PeriodStart.getFullYear() + "-" + PeriodStartMonth + "-" + PeriodStartDay,
          "PeriodEnd": PeriodEnd.getFullYear() + "-" + PeriodEndMonth + "-" + PeriodEndDay,
          "Status": Status,
          }
        })
        .then(response => {
          if(response.ok) {
            response.json()
              .then(body => { 
                dispatch({
                  type: 'UPDATE_VACATION_REQUEST_SUCCESS',
                  updatedVacationRequest: JSON.parse(body)
                });
              })
          }
          else if(response.status === 401) {
            dispatch(setLoginError("Session Expired"))
            dispatch(logout())
          }
          else {
            dispatch({
              type: 'UPDATE_VACATION_REQUEST_FAILURE',
              error: "An Error Occured"
            });
          }
        })
        .catch((error: any) => {
          console.log(error)
        })
    }
}


export function deleteVacationRequest(ID: number) {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: 'DELETE_VACATION_REQUEST_BEGIN'
    });
    
    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/request/' + ID, {
        method: 'DELETE',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`
        },
      })
      .then(response => {
        if(response.ok) {
          response.json()
            .then(body => { 
              dispatch({
                type: 'DELETE_VACATION_REQUEST_SUCCESS',
              });
            })
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'DELETE_VACATION_REQUEST_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function setVacationRequestError(error: string) {
  return (dispatch: any) => {
    dispatch({
      type: 'SET_VACATION_REQUEST_ERROR',
      error: error
    });
  }
}


export function setUpdatedVacationRequestNull() {
  return (dispatch: any) => {
    dispatch({
      type: 'UPDATE_VACATION_REQUEST_SUCCESS',
      updatedVacationRequest: null
    });
  }
}