import { logout, setLoginError } from "./userActions";


export function fetchIneligiblePeriods() {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: 'FETCH_IneligiblePeriods_BEGIN'
    });
    
    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/ineligible', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${getState().userState.currentUser.token}`
        },
      })
      .then(response => {
        if(response.ok) {
          response.json()
            .then(body => { 
              dispatch({
                type: 'FETCH_IneligiblePeriods_SUCCESS',
                ineligiblePeriods: body
              });
            })
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'FETCH_IneligiblePeriods_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function postNewIneligiblePeriod(ModeratorId: number, PeriodStart: Date, PeriodEnd: Date) {
  return (dispatch: any, getState:any) => {
    dispatch({
      type: 'POST_NEW_INELIGIBLE_PERIOD_BEGIN'
    });

    let PeriodStartMonth = (PeriodStart.getMonth() + 1) > 9 ? (PeriodStart.getMonth() + 1) : "0" + (PeriodStart.getMonth() + 1)
    let PeriodEndMonth = (PeriodEnd.getMonth() + 1) > 9 ? (PeriodEnd.getMonth() + 1) : "0" + (PeriodEnd.getMonth() + 1)
    let PeriodStartDay = (PeriodStart.getDate()) > 9 ? (PeriodStart.getDate()) : "0" + (PeriodStart.getDate())
    let PeriodEndDay = (PeriodEnd.getDate()) > 9 ? (PeriodEnd.getDate()) : "0" + (PeriodEnd.getDate())


    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/ineligible', {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`,

        "ModeratorId": ModeratorId.toString(),
        "PeriodStart": PeriodStart.getFullYear() + "-" + PeriodStartMonth + "-" + PeriodStartDay,
        "PeriodEnd": PeriodEnd.getFullYear() + "-" + PeriodEndMonth + "-" + PeriodEndDay
        },
      })
      .then(response => {
        if(response.ok) {
          response.json()
            .then(body => { 
              dispatch({
                type: 'POST_NEW_INELIGIBLE_PERIOD_SUCCESS',
                newIneligiblePeriod: JSON.parse(body)
              });
            })
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'POST_NEW_INELIGIBLE_PERIOD_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}

export function postUserIneligiblePeriod(UserId:number, IneligibleperiodId:number){
  return(dispatch: any, getState:any) =>{
    dispatch({
      type:'POST_NEW_USER_INTELIGIBLE_PERIOD_BEGIN'
    });

    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/userineligibleperiod ',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`,

        "UserId": UserId.toString(),
        "IneligiblePeriodId": IneligibleperiodId.toString()
        },
    })
    .then(response => {
      if(response.ok) {
        response.json()
          .then(body => { 
            dispatch({
              type: 'POST_NEW_USER_INELIGIBLE_PERIOD_SUCCESS',
              newUserIneligiblePeriod: JSON.parse(body)
            });
          })
      }
      else if(response.status === 401) {
        dispatch(setLoginError("Session Expired"))
        dispatch(logout())
      }
      else {
        dispatch({
          type: 'POST_NEW_INELIGIBLE_PERIOD_FAILURE',
          error: "An Error Occured"
        });
      }
    })
    .catch((error: any) => {
      console.log(error)
    })
  }
}


export function deleteIneligiblePeriod(id: number) {
  return (dispatch: any, getState:any) => {
    dispatch({
      type: 'DELETE_INELIGIBLE_PERIOD_BEGIN'
    });

    return fetch('https://tidsbankenfuncs.azurewebsites.net/api/ineligible/' + id, {
        method: 'DELETE',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${getState().userState.currentUser.token}`,
        },
      })
      .then(response => {
        if(response.ok) {
          dispatch({
            type: 'DELETE_INELIGIBLE_PERIOD_SUCCESS',
          });
        }
        else if(response.status === 401) {
          dispatch(setLoginError("Session Expired"))
          dispatch(logout())
        }
        else {
          dispatch({
            type: 'DELETE_INELIGIBLE_PERIOD_FAILURE',
            error: "An Error Occured"
          });
        }
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}


export function setNewIneligiblePeriodToNull() {
  return (dispatch: any) => {
    dispatch({
      type: 'SET_NEW_INELIGIBLEPERIOD_TO_NULL',
    });
  }
}