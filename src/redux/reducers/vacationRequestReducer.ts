

const vacationRequestState = {

    vacationRequests: [],

    newVacationRequest: null,
    postNewVacationRequestIsLoading: false,
    postNewVacationRequestError: null,

    updatedVacationRequest: null,
    updateVacationRequestIsLoading: false,
    updateVacationRequestError: null,

    deleteVacationRequestIsLoading: false,
    deleteVacationRequestError: null,
    deleteVacationRequestWasSuccessfull: false,
  };
  
  
  export function vacationRequestReducer(state = vacationRequestState, action: any) {
  
    switch (action.type) {
      case 'FETCH_VACATION_REQUESTS_SUCCESS':
        return {
          ...state,
          vacationRequests: action.vacationRequests,
        };

      case 'POST_NEW_VACATION_REQUEST_BEGIN':
        return {
          ...state,
          postNewVacationRequestIsLoading: true,
          postNewVacationRequestError: null,
        };
  
      case 'POST_NEW_VACATION_REQUEST_SUCCESS':
        return {
          ...state,
          newVacationRequest: action.newVacationRequest,
          postNewVacationRequestIsLoading: false,
          postNewVacationRequestError: null,
        };
  
      case 'POST_NEW_VACATION_REQUEST_FAILURE':
        return {
          ...state,
          postNewVacationRequestIsLoading: false,
          postNewVacationRequestError: action.error,
        };





      case 'UPDATE_VACATION_REQUEST_BEGIN':
        return {
          ...state,
          updateVacationRequestIsLoading: true,
          updateVacationRequestError: null,
        };
  
      case 'UPDATE_VACATION_REQUEST_SUCCESS':
        return {
          ...state,
          updatedVacationRequest: action.updatedVacationRequest,
          updateVacationRequestIsLoading: false,
          updateVacationRequestError: null,
        };
  
      case 'UPDATE_VACATION_REQUEST_FAILURE':
        return {
          ...state,
          updateVacationRequestIsLoading: false,
          updateVacationRequestError: action.error,
        };





      case 'DELETE_VACATION_REQUEST_BEGIN':
        return {
          ...state,
          deleteVacationRequestIsLoading: true,
          deleteVacationRequestError: null,
          deleteVacationRequestWasSuccessfull: false,
        };
  
      case 'DELETE_VACATION_REQUEST_SUCCESS':
        return {
          ...state,
          deleteVacationRequestIsLoading: false,
          deleteVacationRequestError: null,
          deleteVacationRequestWasSuccessfull: true,
        };
  
      case 'DELETE_VACATION_REQUEST_FAILURE':
        return {
          ...state,
          deleteVacationRequestIsLoading: false,
          deleteVacationRequestError: action.error,
        };





      case 'SET_VACATION_REQUEST_ERROR':
        return {
          ...state,
          postNewVacationRequestError: action.error,
          updateVacationRequestError: action.error
        };
        
      default:
        return state;
    }
  }