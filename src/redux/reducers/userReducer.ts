

const userState: any = {
  currentUser: null,
  requestLoginIsLoading: false,
  requestLoginError: null,

  users: [],
  fetchUsersIsLoading: null,
  fetchUsersError: null,

  password: null,
  postNewUserIsLoading: null,
  postNewUserError: null,

  updatedUser: null,
  updateUserIsLoading: false,
  updateUserError: null,
  updateUserWasSuccessfull: false,

  updateUserPasswordIsLoading: false,
  updateUserPasswordError: null,
  updateUserPasswordWasSuccessfull: false,

  deleteUserIsLoading: false,
  deleteUserError: null,
  deleteUserWasSuccessfull: false,
};


export function userReducer(state = userState, action: any) {

  switch (action.type) {
    case 'REQUEST_LOGIN_BEGIN':
      return {
        ...state,
        requestLoginIsLoading: true,
        requestLoginError: null,
      };

    case 'REQUEST_LOGIN_SUCCESS':
      return {
        ...state,
        currentUser: action.currentUser,
        token: action.token,
        requestLoginIsLoading: false,
        requestLoginError: null,
      };

    case 'REQUEST_LOGIN_FAILURE':
      return {
        ...state,
        requestLoginIsLoading: false,
        requestLoginError: action.error,
      };



      
      
    case 'LOGOUT':
      return {
        ...state,
        currentUser: null,
      };

    case 'SET_CURRENT_USER':
      return {
        ...state,
        currentUser: action.currentUser,
      };

    case 'SET_USERS':
      return {
        ...state,
        users: action.users,
      };





    case 'FETCH_USERS_BEGIN':
      return {
        ...state,
        fetchUsersIsLoading: true,
        fetchUsersError: null,
      };

    case 'FETCH_USERS_SUCCESS':
      return {
        ...state,
        users: action.users,
        fetchUsersIsLoading: false,
        fetchUsersError: null,
      };

    case 'FETCH_USERS_FAILURE':
      return {
        ...state,
        fetchUsersIsLoading: false,
        fetchUsersError: action.error,
      };





    case 'POST_NEW_USER_BEGIN':
      return {
        ...state,
        postNewUserIsLoading: true,
        fetchUsersError: null,
      };

    case 'POST_NEW_USER_SUCCESS':
      return {
        ...state,
        password: action.password,
        postNewUserIsLoading: false,
        postNewUserError: null,
      };

    case 'POST_NEW_USER_FAILURE':
      return {
        ...state,
        postNewUserIsLoading: false,
        postNewUserError: action.error,
      };





    case 'UPDATE_USER_BEGIN':
      return {
        ...state,
        updateUserIsLoading: true,
        updateUserError: null,
        deleteUserError: null,
      };

    case 'UPDATE_USER_SUCCESS':
      return {
        ...state,
        updatedUser: action.updatedUser,
        updateUserIsLoading: false,
        updateUserError: null,
        updateUserWasSuccessfull: action.updateUserWasSuccessfull
      };

    case 'UPDATE_USER_FAILURE':
      return {
        ...state,
        updateUserIsLoading: false,
        updateUserError: action.error,
      };





    case 'UPDATE_USER_PASSWORD_BEGIN':
      return {
        ...state,
        updateUserPasswordIsLoading: true,
        updateUserPasswordError: null,
        updateUserPasswordWasSuccessfull: false
      };

    case 'UPDATE_USER_PASSWORD_SUCCESS':
      return {
        ...state,
        updateUserPasswordIsLoading: false,
        updateUserPasswordError: null,
        updateUserPasswordWasSuccessfull: action.updateUserPasswordWasSuccessfull
      };

    case 'UPDATE_USER_PASSWORD_FAILURE':
      return {
        ...state,
        updateUserPasswordIsLoading: false,
        updateUserPasswordError: action.error,
        updateUserPasswordWasSuccessfull: false
      };





    case 'DELETE_USER_BEGIN':
      return {
        ...state,
        deleteUserIsLoading: true,
        deleteUserError: null,
        updateUserError: null,
        deleteUserWasSuccessfull: false,
      };

    case 'DELETE_USER_SUCCESS':
      return {
        ...state,
        deleteUserIsLoading: false,
        deleteUserError: null,
        deleteUserWasSuccessfull: true,
      };

    case 'DELETE_USER_FAILURE':
      return {
        ...state,
        deleteUserIsLoading: false,
        deleteUserError: action.error,
      };

    

      
    default:
      return state;
  }
}