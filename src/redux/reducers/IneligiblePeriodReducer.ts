

const ineligiblePeriodState = {
  newIneligiblePeriod: null,
  postNewIneligiblePeriodIsLoading: false,
  postNewIneligiblePeriodError: null,

  newUserIneligiblePeriod: null,
  postNewUserInteligiblePeriod: null,
  postNewUserInteligiblePeriodIsLoading: false,
  postNewUserInteligiblePeriodError: null,

  ineligiblePeriods: null,
  fetchIneligiblePeriodsIsLoading: false,
  fetchIneligiblePeriodsError: null,

  deleteIneligiblePeriodIsLoading: false,
};


export function ineligiblePeriodReducer(state = ineligiblePeriodState, action: any) {

  switch (action.type) {
    case 'POST_INELIGIBLE_PERIOD_BEGIN':
      return {
        ...state,
        postNewIneligiblePeriodIsLoading: true,
        postNewIneligiblePeriodError: null,
      };

    case 'POST_NEW_INELIGIBLE_PERIOD_SUCCESS':
      return {
        ...state,
        newIneligiblePeriod: action.newIneligiblePeriod,
        postNewIneligiblePeriodIsLoading: false,
        postNewIneligiblePeriodError: null,
      };

    case 'POST_NEW_INELIGIBLE_PERIOD_FAILURE':
      return {
        ...state,
        postNewIneligiblePeriodIsLoading: false,
        postNewIneligiblePeriodError: action.error,
      };

      case 'POST_NEW_USER_INTELIGIBLE_PERIOD_BEGIN' :
        return{
        ...state,
        postNewUserInteligiblePeriodIsLoading:true,
        postNewIneligiblePeriodError:null,          
      };

      case 'POST_NEW_USER_INTELIGIBLE_PERIOD_SUCCESS' :
        return {
        ...state,
        newUserIneligiblePeriod: action.newUserIneligiblePeriod,
        postNewUserInteligiblePeriodIsLoading:true,
        postNewIneligiblePeriodError:null,        
        };

      case 'POST_NEW_USER_INTELIGIBLE_PERIOD_FAILURE' :
        return {
        ...state,
        postNewUserInteligiblePeriodIsLoading:false,
        postNewIneligiblePeriodError: action.error,        
        };



      case 'FETCH_IneligiblePeriods_BEGIN':
        return {
          ...state,
          fetchIneligiblePeriodsIsLoading: true,
          fetchIneligiblePeriodsError: null,
        };

      case 'FETCH_IneligiblePeriods_SUCCESS':
        return {
          ...state,
          ineligiblePeriods: action.ineligiblePeriods,
          fetchIneligiblePeriodsIsLoading: false,
          fetchIneligiblePeriodsError: null,
        };

      case 'FETCH_IneligiblePeriods_FAILURE':
        return {
          ...state,
          fetchIneligiblePeriodsIsLoading: false,
          fetchIneligiblePeriodsError: action.error,
        };





      case 'DELETE_INELIGIBLE_PERIOD_BEGIN':
        return {
          ...state,
          deleteIneligiblePeriodIsLoading: true,
        };

      case 'DELETE_INELIGIBLE_PERIOD_SUCCESS':
        return {
          ...state,
          deleteIneligiblePeriodIsLoading: false,
        };

      case 'DELETE_INELIGIBLE_PERIOD_FAILURE':
        return {
          ...state,
          deleteIneligiblePeriodIsLoading: false,
        };




        
      case 'SET_NEW_INELIGIBLEPERIOD_TO_NULL' :
        return {
        ...state,
        newIneligiblePeriod: null,       
        };


    default:
      return state;
  }

}