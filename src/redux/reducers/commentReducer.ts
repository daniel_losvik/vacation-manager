

const commentState = {

    comments: [],
    fetchCommentsIsLoading: false,

    newComment: null,
    postNewCommentIsLoading: false,
    postNewCommentError: null,
  };
  
  
  export function commentReducer(state = commentState, action: any) {
  
    switch (action.type) {
      case 'POST_NEW_COMMENT_BEGIN':
        return {
          ...state,
          postNewCommentIsLoading: true,
          postNewCommentError: null,
        };
  
      case 'POST_NEW_COMMENT_SUCCESS':
        return {
          ...state,
          newComment: action.newComment,
          postNewCommentIsLoading: false,
          postNewCommentError: null,
        };
  
      case 'POST_NEW_COMMENT_FAILURE':
        return {
          ...state,
          postNewCommentIsLoading: false,
          postNewCommentError: action.error,
        };

        

      case 'SET_COMMENTS':
        return {
          ...state,
          comments: action.comments,
        };

      case 'SET_FETCH_COMMENTS_IS_LOADING':
        return {
          ...state,
          fetchCommentsIsLoading: action.fetchCommentsIsLoading,
        };

        
      default:
        return state;
    }
  }