import React from 'react';
import './App.css';
import { Route, Redirect } from 'react-router';
import { connect } from 'react-redux';
import { setCurrentUser, fetchUsers } from './redux/actions/userActions'; 
import Navbar from './components/Navbar';
import Login from './pages/login';
import AdminDashboard from './pages/admin/AdminDashboard';
import AdminAddUser from './pages/admin/AdminAddUser';
import UserList from './pages/UserList';
import EditProfile from './pages/EditProfile';
import EditPassword from './pages/EditPassword';
import NewVacationRequest from './pages/NewVacationRequest';
import EditVacationRequest from './pages/EditVacationRequest';
import Calendar from './pages/Calendar';


function App(props: any) {

  let currentUser = props.currentUser;
  if(!currentUser) {
    currentUser = sessionStorage.getItem("currentUser");
    currentUser = JSON.parse(currentUser);

    if(currentUser) {
      props.dispatch(setCurrentUser(currentUser))
    }
  }

  if(currentUser) {
    sessionStorage.setItem("currentUser", JSON.stringify(currentUser));
    props.dispatch(fetchUsers())
    document.body.style.backgroundImage = "";
    return (
      <div className="App">
        <Navbar />
        
        <Route exact path="/"><Calendar /></Route>
        <Route exact path='/Profile' component={EditProfile} />
        <Route exact path='/Profile/ChangePassword' component={EditPassword} />
        <Route exact path='/NewVacationRequest' component={NewVacationRequest} />
        <Route exact path='/VacationRequest/:vrId' component={EditVacationRequest} />
        <Route exact path='/Users' component={UserList} />
        <Route exact path='/User/:id/Calendar' component={Calendar} />
        
        <Route exact path="/Admin/Dashboard">
          {!currentUser.IsAdmin ? <Redirect to="/" /> : <AdminDashboard />}
        </Route>
        <Route exact path="/Admin/AddUser">
          {!currentUser.IsAdmin ? <Redirect to="/" /> : <AdminAddUser />}
        </Route>
        <Route exact path="/Admin/Users">
          {!currentUser.IsAdmin ? <Redirect to="/" /> : <UserList />}
        </Route>
        <Route exact path="/Admin/User/:id/Profile">
          {!currentUser.IsAdmin ? <Redirect to="/" /> : <EditProfile />}
        </Route>
        <Route exact path="/Admin/User/:id/Calendar">
          {!currentUser.IsAdmin ? <Redirect to="/" /> : <Calendar />}
        </Route>
        <Route exact path="/Admin/User/:id/VacationRequest/:vrId">
          {!currentUser.IsAdmin ? <Redirect to="/" /> : <EditVacationRequest />}
        </Route>
      </div>
    );
  }
  else {
    document.body.style.backgroundImage = `url(${require("./assets/abstract-summer-background-vector.jpg")})`
    return (
      <div className="App">
        <Login />
      </div>
    );
  }
}


function mapStateToProps(state: any) {
  return {
    currentUser: state.userState.currentUser
  };
}
export default connect(mapStateToProps)(App);

